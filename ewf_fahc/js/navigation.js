/********************************************************************************
 * @brief																		*
 * 		Navigation Functions													*
 *																				*
 * @author																		*
 *		Bill Sears\n															*
 *		Aceso\n																	*
 *		http://www.aceso.com\n													*
 *																				*
 * @modified																	*
 * 		Tami Seago, 02/05/2013, add info and error handling						*
 ********************************************************************************/

function pause( iMilliseconds )
{
  var sDialogScript = 'window.setTimeout( function () { window.close(); }, ' + iMilliseconds + ');';
}

function discharged()	{
	var version = $("#K_version").text();
	if (version=='ENSEO')	{
		//Nimbus.reload(true);
	}
	window.location = 'discharged.html';		
}

function reloadapp()	{
	var version = $("#K_version").text();
	if (version=='ENSEO')	{
		Nimbus.reload(true);
	}
	window.location = 'index.html';		
}

function changeportal()	{

	var ewf 		= ewfObject();	
	var version = $("#K_version").text();
	if (version=='ENSEO')	{
		Nimbus.reload(true);
	}
	window.location = ewf.ewflocation;		
}


function keypressed(keyCode)	{
	
	var keys = Array();
	
	keys[13] = 'ENTER';
	keys[35] = 'END';
	keys[36] = 'HOME';
	keys[37] = 'LEFT';
	keys[38] = 'UP';
	keys[39] = 'RIGHT';
	keys[40] = 'DOWN';
	keys[33] = 'CHDN';
	keys[34] = 'CHUP';
	
	keys[48] = '0';
	keys[49] = '1';
	keys[50] = '2';
	keys[51] = '3';
	keys[52] = '4';
	keys[53] = '5';
	keys[54] = '6';
	keys[55] = '7';
	keys[56] = '8';
	keys[57] = '9';	
	
	keys[61446] = 'ENTER';
	keys[61444] = 'LEFT';
	keys[61442] = 'UP';
	keys[61445] = 'RIGHT';
	keys[61443] = 'DOWN';
	
	keys[61447] = 'MENU';
	keys[216] = 'MENU';
	
	keys[61441] = 'POWR';	// power
	keys[61507] = 'POWR';	// power
	keys[61508] = 'POWR';	// power

	keys[61521] = 'CC';		// closed caption
	
	keys[61483] = 'CHUP';	// channel+
	keys[61484] = 'CHDN';	// channel-
	//keys[61449] = 'VOLU';	// volume+
	//keys[61448] = 'VOLD';	// volume-
	
	keys[61464] = 'PLAY';	// pause
	keys[61465] = 'STOP';	// stop
	keys[61464] = 'PAUS';	// pause
	keys[61467] = 'RWND';	// rewind
	keys[61468] = 'FFWD';	// fast forward		
	// reset the idle timeout	resetIdleTimer();
	var key = keys[keyCode];
	
	if (!key)	{
		msg('keypress not processed... Actual Value: ' + keyCode + ' Translated Value: ' + key);
		return false;
	}

	var panel   = $("#K_panel").attr("class");
	var version = $("#K_version").text();

	var currmenu    = $("#K_menu").attr("class");
	var currsubmenu = $("#K_submenu").attr("class");
	
	var subpanel   = $("#K_subpanel").attr("class");
	var submenu    = $("#K_submenu").attr("class");
	var currchoice = $("#K_choice").attr("class");
	var active = $("#K_slider").text();
	var count = $("#K_slider").attr("class");
	
	if (version=='ENSEO'||version=='TCM'||version=='NEBULA')	{
		if (keyCode==61464||keyCode==61465||keyCode==61467||keyCode==61468||keyCode==61447)	{
			msg('KEYPRESSED Actual Value: ' + keyCode + ' Translated Value: ' + key );	
		}
	}
	
	if (key=='POWR' ||	key=='END')	{
		reloadmain();
		var onoff = checkPower();
					
		if (onoff=='ON')	{
		
			if (panel=='video')	{				
				stopVideo();
				$("#K_panel").removeClass('video');
				$("#K_panel").addClass('tertiary');
				$("#tertiaryLeft").show();
				$("#tertiaryRight").show();		
				$("#tertiary").show();					
			}	else
				if (panel=='tv')		{					
					stopTV();
					var grid = $("#K_grid").attr("class");
					$("#K_panel").removeClass('tv');
					$("#K_panel").addClass(grid);
					$("#watchtv").show();
					rebuildgrid();							
				}	else
					if (panel=='scenictv')		{					
						stopVideo();
						$("#K_grid").removeClass('scenictv');
						$("#K_panel").removeClass('scenictv');
						$("#K_panel").addClass('primary');
						$("#primary").show();						
					}
		}	
		return false;	
	}
	
	if (key=='CC')	{
		setCC();
		return true;	
	}
	
	if (key=='MENU'||key=='HOME')	{
		if (panel=='video'||panel=='scenictv')	stopVideo();
		if (panel=='watchtv')	{
			stopTV();
			gotomainmenu();		
		}
		if (panel=='video')	{		
			$("#K_panel").removeClass('video');
			$("#K_panel").addClass('tertiary');
			$("#tertiaryLeft").show();
			$("#tertiaryRight").show();		
			$("#tertiary").show();	
			return true;
		}	else
			if (panel=='tv')	{		
				var grid = $("#K_grid").attr("class");
				$("#K_panel").removeClass('tv');
				$("#K_panel").addClass(grid);
				$("#watchtv").show();
				rebuildgrid();
				return true;
			}	else
				if (panel=='scenictv')	{
					$("#K_grid").removeClass('scenictv');
					$("#K_panel").removeClass('scenictv');
					$("#K_panel").addClass('primary');
					$("#primary").show();
					return true;
				}	else
					if (panel=='watchtv')	{
						cleanupTV();
						$("#K_panel").removeClass('watchtv');
						$("#K_panel").addClass('primary');
						$("#K_panel").text('');
						$("#watchtv").hide();
						$("#primary").show();
						return true;
					}	else
						if (panel=='musicradio')	{
							cleanupTV();
							$("#K_panel").removeClass('musicradio');
							$("#K_panel").addClass('primary');
							$("#K_panel").text('');
							$("#watchtv").hide();
							$("#primary").show();
							return true;
						}

		var subpanel = $("#K_subpanel").attr("class");
		
		gotomainmenu();
		return true;

	}		
	

	if (panel=='primary')	{	    
		var currmenu    = $("#K_menu").attr("class");
		var currsubmenu = $("#K_submenu").attr("class");
		
		$("#images").css( { "background": "none" } ); 
		$("#pictures").hide();		
		if(currmenu == 'tv' && (key == 'RIGHT' || key =='ENTER')){
			$("#K_submenu").removeAttr("class");
			$("#K_submenu").addClass('watchtv');
			currsubmenu = $("#K_submenu").attr("class");	
			gotosecondary();												
			} else 
		if (currmenu!='')	{
			if(currsubmenu!='') {
			if (key=='UP')	{
				var currsubmenu = $("#submenuitem."+currsubmenu).attr("rev");
				gotosubmenu(currsubmenu);
			}	else
				if(key=='DOWN')	{
					var currsubmenu = $("#submenuitem."+currsubmenu).attr("rel");
					gotosubmenu(currsubmenu);
					}	else
						if(key=='LEFT')	{
							gotomenu(currmenu);	
							$("#menuitem."+currmenu).removeClass('hover');	
							$("#menuitem."+currmenu).addClass('active');								
							
						}		else
							if(key=='ENTER' || key=='RIGHT')	{	
							
								if(currsubmenu=='back') {
									gotomenu(currmenu);										
								} else {
									if (currsubmenu)
										gotosecondary();
									}
								}
			} else {
			
			if (key=='UP')	{
				var menu = $("#menuitem."+currmenu).attr("rev");
				gotomenu(menu);
			}	else
				if(key=='DOWN')	{
					var menu = $("#menuitem."+currmenu).attr("rel");					
					gotomenu(menu);
				}	else
					if(key=='RIGHT' || key =='ENTER')	{
						if (currsubmenu!='')	{
							var currsubmenu = $("#submenuitem."+currsubmenu).attr("rev");
						}
							gotosubmenu(currsubmenu);
							
					}	else
						if(key=='LEFT')	{
							if (currsubmenu!='')	{
								var currsubmenu = $("#submenuitem."+currsubmenu).attr("rel");
							}
							gotosubmenu(currsubmenu);
							}
					}
			return true;
		}
		
		var menu = $("#K_menufirst").attr("class");
		
		gotomenu(menu);
		
		return true;
	}

		var subpanel   = $("#K_subpanel").attr("class");
		var submenu    = $("#K_submenu").attr("class");
		var currchoice = $("#K_choice").attr("class");
		var active = $("#K_slider").text();

		if (panel=='secondary')	{		
			var subpanel   = $("#K_subpanel").attr("class");
			var submenu    = $("#K_submenu").attr("class");
			var currchoice = $("#K_choice").attr("class");

			if (subpanel=='selections')	{		
				var currselection = $("#K_selection").attr("class");	
				if(key=='UP')	{
					var currselection = $("#K_selection").attr("class");
					var selection = $("#selection."+currselection).attr("rev");						
					
					if(selection=='prev'){							
						moreselections(key);
						var selection = $("#K_selectionfirst").attr("class");
					} else {
						var selection = $("#selection."+currselection).attr("rev");
					}
					gotoselections(selection);
				}	else
					if(key=='DOWN')	{
						var currselection = $("#K_selection").attr("class");
						var selection = $("#selection."+currselection).attr("rel");						
						if(selection=='more'){							
							moreselections(key);
							var selection = $("#K_selectionfirst").attr("class");
						} else {
						var selection = $("#selection."+currselection).attr("rel");
						}
						gotoselections(selection);
					}	else
						if(key=='LEFT')	{
							removeselections();
						}	else
							if(key=='ENTER' || key=='RIGHT')	{
								gotoselection(currselection);
							}
		return true;			
	}
		
		if (submenu=='movies'&&(currchoice!='more'&&currchoice!='back'&&currchoice!='mainmenu'))	{
			
			var initiate = $("#K_carousel").text();
			if(initiate=='initiate')	{
				if(key=='RIGHT'||key=='ENTER')	{
					initiateCarousel();
					return true;
				}	else
				if (key=='LEFT')	{
					return true;
				}
			}			
			if(key=='LEFT'||key=='RIGHT')	{
				shiftCarousel(key);
				return true;
			}	else					
				if(key=='ENTER')	{
					gotoplaymovie();
					return true;
				}
		}

		if(key=='UP')	{
			var choice = $("#choice."+currchoice).attr("rev");
			if(choice == 'blank') {
				currchoice = 'blank';
				choice = $("#choice."+currchoice).attr("rev");
			}								
			nextchoice(choice);
		}	else
			if(key=='DOWN')	{
				var choice = $("#choice."+currchoice).attr("rel");
				if(choice == 'blank') {
					currchoice = 'blank';
					choice = $("#choice."+currchoice).attr("rel");
				}								
				nextchoice(choice);
			}	else					
				if(key=='LEFT')	{
					$("#K_choice").removeAttr("class");
					$("#K_choice").addClass("back");
					gotochoice();
					//nextpanel(key);
				}	else					
					if(key=='RIGHT')	{
						nextpanel(key);
					}	else		
						if(key=='ENTER')	{							
							gotochoice();
						}
			
		return true;					
	}
	
	
	if (panel=='tertiary')	{
	
		var currcontrol = $("#K_control").attr("class");
		
		if(key=='UP')	{
			var control = $("#control."+currcontrol).attr("rev");
			gotocontrols(control);
		}	else
			if(key=='DOWN')	{
				var control = $("#control."+currcontrol).attr("rel");
				gotocontrols(control);
			}		else
				if(key=='LEFT'||key=='RIGHT')	{
					nextmovie(key);
					return true;
				}	else	
					if(key=='ENTER')	{
						gotocontrol();
					}
			
		return true;						
	}
	
	if (panel=='watchtv'||panel=='musicradio')	{	
				
		if (key=='LEFT')	{
			moveCurrent(key);
		}	else
			if(key=='RIGHT')	{
				moveCurrent(key);
			}	else
				if(key=='UP')	{
					moveCurrent(key);
				}	else
						if(key=='DOWN')	{
						moveCurrent(key);
					}		else
						if(key=='ENTER')	{
							gotochannel();
						}
			
		return true;						
	}
		
	if (panel=='tv')	{	

		if(key=='CHUP'||key=='CHDN')	{
			updnChannel(key);
		}
		
		if (key=='0'||key=='1'||key=='2'||key=='3'||key=='4'||key=='5'||key=='6'||key=='7'||key=='8'||key=='9')	{
			keyChannel(key);	
		}
			
		return true;						
	}	
	
	if (panel=='video')	{
		
		if (key=='PLAY'||key=='STOP'||key=='PAUS'||key=='RWND'||key=='FFWD')	{
			var submenu = $("#K_submenu").attr("class");
			if (key=='FFWD'&&(submenu=='myprograms'||submenu=='allprograms'))		
				return true;
			updateVideo(key);
			if (key=='STOP')	{
				$("#K_panel").removeClass('video');
				$("#K_panel").addClass('tertiary');
				$("#tertiaryLeft").show();
				$("#tertiaryRight").show();		
				$("#tertiary").show();	
			}			
			return true;
		}
		
	}

	msg('keypress not caught/processed... Actual Value: ' + keyCode + ' Translated Value: ' + key);
	return true;
}


function buildmenu()	{
	
	// initialize index.html
	
	var indexHTML = initIndexHTML();
	$("#kookies").html(indexHTML.kookies);
	$("#secondary").html(indexHTML.secondary);
	$("#tertiary").html(indexHTML.tertiary);
	$("#watchtv").html(indexHTML.watchtv);
	
	// build menu	
	$("#K_panel").addClass('primary');
	
	var patientHTML = getPatientHTML();
	$("#patient").html(patientHTML.patient);
	$("#mainmenu").html('<p class="mainmenu">MAIN MENU</p>');
	
	// we already checked discharge in getPatientHTML()	
	//checkDischarge();
	setInterval( "checkDischarge()", 120000 );

	datetime();
	setInterval( "datetime()", 60000 );
	
	var menuHTML = getMenuHTML();
	$("#pictures").html(menuHTML.menuimage);
	
	$("#K_menufirst").addClass(menuHTML.menufirst);
	$("#K_menufirst").text(menuHTML.menulabel);
	
	$("#K_menu").addClass('');
	
	$("#pictures").html(menuHTML.menuimage);
	$("#menu").html(menuHTML.menu);
	$("#menuitems").html(menuHTML.menuitems);
	
	blink('#pictures #mainpanel img.arrows');
	setInterval( "blink('#pictures #mainpanel img.arrows')", 3000 );
	
	var version = $("#K_version").text();
	if (version=='TEST'||version=='ENSEO'||version=='TCM')	{
		// we have delayed the loading of videos to the point when user goes into submenu
		var productofferingDATA 	= getProductOfferingDATA();
		var scenictvcategoryDATA 	= getMovieCategoryDATA('scenicTV');
		var channelsDATA 			= getChannelsDATA();	// need to comment out to implement
		msg("PO and channel data loaded.");
	}	
	
	$("#pictures").show();
	$("#primary").show();
	$(document).ready(function() {		
		//$("#loading").delay(2000).fadeOut('slow');
		$('#loading').hide();	
	});		
		
	$("#K_versionoverlay").text('');
	
	var pathname = window.location;
	if (pathname=='http://10.238.78.222:9090/ewf_test_test/index.html')	$("#K_versionoverlay").text('TEST');
	var version = $("#K_version").text();
	var client = $("#K_client").text();
	var portal = $("#K_portal").text();
	$("#K_versionoverlay").text('Version ' + version + ' Portal ' + portal + ' Client ' + client);
	
					
	var versionoverlay = $("#K_versionoverlay").text();
	//if (versionoverlay)	{
	//	$("#testoverlay").text(versionoverlay);
	//	$("#testoverlay").show();
	//}
		
	msg("buidmenu() completed");
	return;
}

function blink(div)	{
	
	$(div).delay(200).fadeTo(100,0.5).delay(200).fadeTo(100,1).delay(200).fadeTo(100,0.5).delay(200).fadeTo(100,1).delay(200).fadeTo(100,0.5).delay(200).fadeTo(100,1).delay(200).fadeTo(100,0.5);
}

// primary functions...

function gotomenu(menu)	{

	var prevmenu = $("#K_menu").attr("class");

	if (prevmenu != '')	{
		$("#menuitem."+prevmenu).removeClass('hover');	
		$("#K_menu").removeAttr("class");
		$("#K_menu").text('');
		$("#K_submenu").removeAttr("class");
		$("#K_submenu").text('');
		$("#K_submenufirst").removeAttr("class");
		$("#K_submenufirst").text('');
		$("#menuitem."+prevmenu).removeClass('active');	
		$("#submenu."+prevmenu).html('');		
		$("#pictures").hide();
	} else {
		var menu = $("#K_menufirst").attr("class");
	}
		
	var submenuHTML = getSubmenuHTML(menu);	
	$("#pictures").html(submenuHTML.submenuimage);
			
	$("#K_submenufirst").addClass(submenuHTML.submenufirst);
	$("#K_submenufirst").text(submenuHTML.submenulabel);
		
	$("#K_menu").addClass(menu);
	var menulabel = $("#menuitem."+menu).text();
	$("#K_menu").text(menulabel);
	
	var image = '<img src="images/'+$("#menuitem."+menu).attr("title")+'" />';

	$("#menuitem."+menu).removeClass('hover');	
	$("#menuitem."+menu).addClass('active');

	$("#submenu."+menu).html(submenuHTML.menuitems).show();
	$("#pictures").show();
	
	return;
}

function gotosubmenu(submenuitem)	{

	var currmenu    = $("#K_menu").attr("class");
	var currsubmenu = $("#K_submenu").attr("class");

	var prevsubmenu = $("#K_submenu").attr("class");
	
	if (prevsubmenu != '')	{
		$("#K_submenu").removeAttr("class");
		$("#K_submenu").text('');
		$("#submenuitem."+prevsubmenu+" span").removeClass('active');
	} else {
		var submenuitem = $("#K_submenufirst").attr("class");
	}
		
	$("#K_submenu").addClass(submenuitem);
	var submenulabel = $("#submenuitem."+submenuitem+' span').text();
	$("#K_submenu").text(submenulabel);
	$("#submenuitem."+submenuitem+" span").addClass('active');
	$("#submenuitem.back").show();
	if(submenuitem=='urgenthelp') {
		$("#images").css( { "background": "block" } ); 
		$("#images").show();
		$("#pictures").css( { "background": "block" } );
		$("#pictures").show();
	}
	
	$("#menuitem."+currmenu).removeClass('hover');	
	$("#menuitem."+currmenu).removeClass('active');								
	$("#menuitem."+currmenu).addClass('hover');
	return;
}

// secondary functions...

function gotosecondary()	{

	var menu    = $("#K_menu").attr("class");
	var submenu = $("#K_submenu").attr("class");
	var submenulabel = $("#K_submenu").text();
	var version = $("#K_version").text();

	document.getElementById("secondary").style.zIndex=700;
	
	var loadcount = $("#K_secondaryload").text();	

	$("#K_panel").removeClass('primary');
	$("#K_panel").addClass('secondary');
	
	$("#K_choice").removeAttr("class");
	$("#K_choice").text('');
	$("#K_choicefirst").removeAttr("class");
	$("#K_choicefirst").text('');

	var menu = $("#K_menu").attr("class");
	var menulabel = $("#K_menu").text();
	
	
	
	// delayed loading logic for data in submenu
	switch(submenu) {
		case 'myprograms':
			getmyprogramsDATA();
			break;
		case 'allprograms':
			getmyprogramsDATA();
			getAllProgramsDATA();
			break;
		case 'movies':
			getmyprogramsDATA();
			getMovieCategoryDATA('movies');	
			break;
		default:
			break;
	}
	
	var choicesHTML = getLevelOneHTML(submenu);

	$("#secondaryTitle").html('');
	
	if(menu == 'childrenshospital') {
		$("#secondaryTitle").html('<p class="menu">&nbsp;</p>');
		$("#secondaryTitle").html('<p class="submenu">&nbsp;</p>');
		$("#secondaryTitle").html('<p class="menu"></p><p class="submenu"></p><p img="image1"><img class="image1" src="./images/VCH_usnews-badges-sm.png" </p></div>');
	} else {
		if(choicesHTML.attr == 'undefined' || (choicesHTML.attr == null)) { 
			var label = '';
			if(submenu != 'movies') {
				label = label + '<p class="menu">'+menulabel+'</p>';	
			} 
			label = label + '<p class="submenu">'+submenulabel+'</p>';
			$("#secondaryTitle").html(label);
		} else { 
			$("#secondaryTitle").html(choicesHTML.attr);
		}
	}
		
			
	$("#K_panel").text(choicesHTML.background);
	$("#K_choice").addClass(choicesHTML.choicefirst);
	$("#K_choice").text(choicesHTML.choicelabel);
	$("#K_choicefirst").addClass(choicesHTML.choicefirst);
	$("#K_choicefirst").text(choicesHTML.choicelabel);
	
	$("#choices").html(choicesHTML.choices);
	$("#choice."+choicesHTML.choicefirst+" span").addClass('active');
	$("#secondaryRight").html(choicesHTML.choiceimage);
	$("#secondaryRight").hide();
	$("#secondaryHead").hide();
	var grid = choicepanel(choicesHTML.choicepanel);
	
	if(grid) return;	

	$("#secondary").addClass(choicesHTML.background);
	if(menu == 'childrenshospital') {
		$("#secondary").addClass('childrenshospital');
	}
	$("#secondary").addClass(choicesHTML.background);
	
	if(choicesHTML.background != null && choicesHTML.background != 'myprograms' && choicesHTML.background != 'allprograms') {		
			$("#secondary").css( { "background-image": "url(./images/"+choicesHTML.background+")","background-position": "top right","background-repeat": "no-repeat" } );  
	}

	if(submenu == 'myprograms') {
		$("#secondary.myprograms").css( { "background-image": "url(./images/bk_fa_education-NEW.jpg)","background-position": "top right","background-repeat": "no-repeat" } );  
	} else 
		if(menu == 'childrenshospital') {
			$("#secondary").css( { "background-image": "url(./images/bk_fa_secondlevel_vch-NEW.jpg)","background-position": "top right","background-repeat": "no-repeat" } );  
		} else 	
			if(submenu == 'allprograms') {
				$("#secondary.allprograms").css( { "background-image": "url(./images/bk_fa_education-NEW.jpg)","background-position": "top right","background-repeat": "no-repeat" } );  
			} else 
				if(submenu == 'movies') {
					$("#secondary.movies").css( { "background-image": "url(./images/bk_fa_secondlevel1-NEW.jpg)","background-position": "top right","background-repeat": "no-repeat" } );  			
				} else {
					$("#secondary").css( { "background-image": "url(./images/bk_fa_secondlevel1-NEW.jpg)","background-position": "top center","background-repeat": "no-repeat" } );  
				}		

	$("#secondaryLeft").show();
	$("#secondaryRight").show();
	$("#secondaryHead").show();	
	$("#secondary").show();
	$(document).ready(function() {	
		if(version=='TCM'||version=='TEST') {
			$("#primary").delay(100).fadeOut('fast');
		} else {
		if(loadcount==1 || submenu=='movies') {
			$("#primary").delay(400).fadeOut('slow');
		} else {
			$("#primary").delay(100).fadeOut('fast');
		}
		}
	});
	loadcount++;
	$("#K_secondaryload").text(loadcount);	

	//$("#menuitem."+menu).delay(600).removeClass('hover');	
	
}

function morechoices()	{
	$("#K_choice").removeAttr("class");
	$("#K_choice").text('');
	$("#K_choicefirst").removeAttr("class");
	$("#K_choicefirst").text('');

	var submenu = $("#K_submenu").attr("class");
	var choicelast = $("#choice.more").attr("rev");

	var choicesHTML = getLevelOneHTML(submenu,choicelast);
	
	$("#K_panel").text(choicesHTML.background);
	$("#K_choice").addClass(choicesHTML.choicefirst);
	$("#K_choice").text(choicesHTML.choicelabel);
	$("#K_choicefirst").addClass(choicesHTML.choicefirst);
	$("#K_choicefirst").text(choicesHTML.choicelabel);
	
	$("#choices").html(choicesHTML.choices);
	$("#choice."+choicesHTML.choicefirst+" span").addClass('active');
	$("#secondaryRight").html(choicesHTML.choiceimage);
		
	choicepanel(choicesHTML.choicepanel);
	
	return;
}

function nextchoice(choice)	{
	
	var prevchoice = $("#K_choice").attr("class");
	
	$("#K_choice").removeAttr("class");
	$("#K_choice").text('');
	$("#choice."+prevchoice+" span").removeClass('active');

	$("#K_choice").addClass(choice);
	var choicelabel = $("#choice."+choice+' span').text();

	$("#K_choice").text(choicelabel);	
	
	$("#choice."+choice+" span").addClass('active');
	if ($("#choice."+choice).attr("title")>'' && $("#choice."+choice).attr("title") != 'undefined')
		$("#secondaryRight").html('<div id="choicepanel"><img src="images/'+$("#choice."+choice).attr("title")+'" /></div>');

	if(choice!='more'&&choice!='back'&&choice!='mainmenu') {
		choicepanel(choice);
	}
	return;
}

function choicepanel(choice)	{
 
	var submenu = $("#K_submenu").attr("class");

	var options = getLevelTwo(choice);	
	if (options.tag=='EMPTY1'||options.tag=='EMPTY2'||options.tag=='EMPTY3')	{	
		if (submenu == 'myprograms') {
			var commentsHTML = getCommentsHTML();			
			$("#secondaryRight").html(commentsHTML.choicepanel);						
		}
		return;
	}
	
	$("#secondaryRight").css( { "background": "none" } ); 

	$("#secondaryRight #choicepanel").html('');
	
	if (options.type=='panel')	{
		//var choiceHTML = getLevelTwoHTML(choice);
		var choiceHTML = getSelectionsHTML(choice);
		var commentsHTML = getCommentsHTML();					
		$("#secondaryRight").html(choiceHTML.selections + commentsHTML.choicepanel);		

	}	else
		if (options.type=='video')	{
			var commentsHTML = getCommentsHTML();			
			$("#secondaryRight").html(commentsHTML.choicepanel);						
			
		}	else	
			if (options.type=='image')	{
				var imageHTML = getImageHTML(choice,options);
				$("#secondaryRight").html(imageHTML.choicepanel);
			}	else	
				if (options.type=='carousel')	{
					$("#K_carouseltotal").text('');
					$("#K_carousel").text('initiate');
					
					var carouselHTML = getCarouselHTML(choice);
					var carouseltotal = carouselHTML.total;
					$("#K_carouseltotal").text(carouseltotal);					
					var carouselpages = carouselHTML.pages;
					
					$("#K_carouselpages").text(carouselpages);					
					$("#secondaryRight").html(carouselHTML.carousel);
					var title = $("#carouselPosters img.current").attr("title");
					$("#carouselTitle").html('<p style="display:none">'+title+'</p>');										
							
				}	else
				
					if (options.type=='page' || options.type=='slider')	{
						var infoHTML = getPageHTML(options);
						$("#secondaryRight").html(infoHTML.choicepanel);						
						if(options.type=='slider') {
							$("#K_slider").removeAttr("class");		//number of sliders
							$("#K_slider").text('');				//current slider	\		
							var acount = infoHTML.count;
							$("#K_slider").addClass(acount.toString());		//number of sliders
							$("#K_slider").text(infoHTML.active);		//current slider	
//msg('count ' + $("#K_slider").attr("class") + ' infoHTML.count ' + infoHTML.count);	
						}
					}	else
					
					if (options.type=='info')	{
						var infoHTML = getInfoHTML(options);
						$("#secondaryRight").html(infoHTML.choicepanel);
						$("#secondaryRight").css( { "background-image": "url(./images/"+infoHTML.background+")","background-position": "top right","background-repeat": "no-repeat" } ); 
					}	else
					
						if (options.type=='slider')	{
							$("#K_slider").removeAttr("class");		//number of sliders
							$("#K_slider").text('');				//current slider						
							var slidesHTML = getSlidesHTML(options);
							$("#K_slider").addClass(slidesHTML.count);		//number of sliders
							$("#K_slider").text(slidesHTML.active);		//current slider
							$("#secondaryRight").html(slidesHTML.sliders);				
							
						}	else
						
							if (options.type=='grid')	{
								//msg(options.tag);
								buildgrid(options.tag);	
								return options.type;
							}			
				
	return;
}

function refreshChoice()	{

}

function gotochoice()	{

	var currchoice = $("#K_choice").attr("class");
	var currmenu    = $("#K_menu").attr("class");
	var currsubmenu = $("#K_submenu").attr("class");

	if (currchoice=='more')	{
		morechoices();		
	} else
		if (currchoice=='back')	{
			
			var background = $("#K_panel").text();

			$("#secondary").removeClass(background).hide();
			$("#primary").show();
			$("#K_panel").removeClass('secondary').text('');
			$("#K_panel").addClass('primary');
			var submenu = $("#K_submenu").attr("class");
			$("#menuitem."+currmenu).removeClass('hover');	
			$("#menuitem."+currmenu).removeClass('active');								

			gotosubmenu(submenu);
			
		} else	
			if (currchoice=='mainmenu')	{	
				
				var background = $("#K_panel").text();
				$("#secondary").removeClass(background).hide();
				$("#primary").show();
				$("#K_panel").removeClass('secondary').text('');
				$("#K_panel").addClass('primary');
				var menu = $("#K_menufirst").attr("class");
				gotomenu(menu);
				
			} else {
			
					var options = getLevelTwo(currchoice);
					if (options.tag=='EMPTY1'||options.tag=='EMPTY2'||options.tag=='EMPTY3')	{	
						return;
					}
					if (options.type=='video')	{
						gotoplayvideo(options);
						}	else					
							if (options.type=='panel')	{
								buildselections(options);  //change this to drop background...
							}	else	{
									return false;
								}	
				}
	return;
}

function nextpanel(key)	{
	
	var count  = $("#K_slider").attr("class");
	//msg('in nextpanel count-' + count);
	if (count==0)
		return;
	
	var active = $("#K_slider").text();
	var ondeck = active;
	if (key=='LEFT')	{
		ondeck--;
		if (ondeck == 0)
			ondeck = count;
	}	else
		if (key=='RIGHT')	{
			ondeck++;
			if (ondeck > count)
				ondeck = 1;
		}	
	$("#K_slider").text(ondeck);

	$("#secondary div#secondaryRight div#slides div#slide.slide"+active).css("display","none");
	$("#secondary div#secondaryRight div#slides div#slide.slide"+ondeck).css("display","block");

	return;
}



function initiateCarousel()	{
	
	$("#K_carousel").text('');
	$("#secondary div#secondaryLeft div#choices a span.active").css("color","#344e9b");
	$("#secondary div#secondaryRight div#carousel div#carouselTitle").show();
	var title = $("#carouselPosters img#1").attr("title");
	$("#secondary div#secondaryRight div#carousel div#carouselTitle").html('<p>'+title+'</p>');
	
	$("#carouselPosters img#1").removeClass("currentx");
	$("#carouselPosters img#1").addClass("current");

	//$("#carouselPosters img#1").css({ "background": "#689142", "height": "310px", "width": "225px", "margin": "0px 0px 0px 0px", "display": "block",  "padding": "45px 30px 30px 30px"}).addClass("current");
	var id = $("#carouselPosters img.current").attr("id");						
	$("#K_carouselcurrentpage").text(1);										

	//$("#secondary div#secondaryRight div#carousel div#carouselOverlay").fadeIn(500);
	//$("#secondary div#secondaryRight div#carousel div#carouselMessage").html('<p>Press Select to Watch a Movie</p>');	
	
}

function shiftCarousel(direction)	{

	var ids = Array();
	
	var id   = $("#carouselPosters img.current").attr("id");
	var total = $("#K_carouseltotal").text();
		
	var ondeck = id;
	if (direction=='LEFT')	{
		ondeck--;
		if (ondeck == 0)
			ondeck = count;
	}	else
		if (direction=='RIGHT')	{
			ondeck++;
			if (ondeck > total)
				ondeck = 1;
		}	
	
	if(ondeck%3 == 1 && (direction=='RIGHT')) {
		nextmoviepanel(direction);
	} else  {
		if(ondeck%3 == 0 && (direction=='LEFT')) {
			nextmoviepanel(direction);
		}
	}
	
	
	//$("#carouselPosters img#"+id).css({ "background-image": "url(images/mov_poster_BK1.png)", "height": "310px","width": "225px","margin": "45px 15px 15px 15p","padding": "15px 15px 1px 15px","display": "block"}).addClass("currentx"); 
	//$("#carouselPosters img#"+ondeck).css({ "background": "#689142", "height": "310px", "width": "225px", "margin": "0px 0px 0px 0px", "padding": "45px 30px 30px 30px","display": "block"}).addClass("current");

	$("#carouselPosters img#"+id).removeClass("current");
	$("#carouselPosters img#"+ondeck).removeClass("currentx");

	$("#carouselPosters img#"+id).addClass("currentx");
	$("#carouselPosters img#"+ondeck).addClass("current");

	
	var title = $("#carouselPosters img#"+ondeck).attr("title");
	$("#secondary div#secondaryRight div#carousel div#carouselTitle").html('<p>'+title+'</p>');

	return;
}


function nextmoviepanel(key)	{
	
	var count  = $("#K_carouselpages").text();

	if (count==0)
		return;
	
	var active = $("#K_carouselcurrentpage").text();
	var ondeck = active;
	
	if (key=='LEFT')	{
		ondeck--;
		if (ondeck == 0)
			ondeck = count;
	}	else
		if (key=='RIGHT')	{
			ondeck++;
			if (ondeck > count)
				ondeck = 1;
		}	

		
	$("#K_carouselcurrentpage").text(ondeck);
	$("#secondary div#secondaryRight div#carousel div#carouselPosters.page"+active).css("display","none");
	$("#secondary div#secondaryRight div#carousel div#carouselPosters.page"+ondeck).css("display","block");

	return;
}


function moveCarousel(ids,direction)	{
	$("#secondary div#secondaryRight div#carousel div#carouselTitle").hide();
	$("#carouselPosters img.current").removeClass("current");
	
	
			
	if (direction=='LEFT')	{
		
		//$("#carouselPosters img#"+ids[0]).hide().css({ "margin-top": "50px","margin-left": "25px","width": "120px","height": "165px","padding-top": "4px","padding-right": "4px","padding-bottom": "0px","padding-left": "4px"});
	} else
		if (direction=='RIGHT')	{
			//$("#carouselPosters img#"+ids[0]).css({ "margin-top": "50px","margin-left": "25px","width": "120px","height": "165px","padding-top": "4px","padding-right": "4px","padding-bottom": "0px","padding-left": "4px" }).addClass("current");
		}		
}
// tertiary functions...

function gotoplayvideo(options)	{
	$("#tertiary").removeAttr("class");
	$("#K_panel").removeClass('secondary');
	$("#K_panel").addClass('tertiary');

	$("#K_control").removeAttr("class");
	$("#K_control").text('');
	$("#K_controlfirst").removeAttr("class");
	$("#K_controlfirst").text('');

	var videoHTML = getVideoHTML(options);
	
	$("#tertiaryTitle.menu").html(videoHTML.menulabel);	
	$("#tertiaryTitle.submenu").html(videoHTML.submenulabel);		
	
	$("#tertiaryRight").html(videoHTML.choicepanel);						
	
	$("#K_control").addClass(videoHTML.controlfirst);
	$("#K_control").text(videoHTML.controllabel);
	$("#K_controlfirst").addClass(videoHTML.controlfirst);
	$("#K_controlfirst").text(videoHTML.controllabel);
	
	$("#controls").html(videoHTML.controls);
	$("#controlimage").html(videoHTML.controlimage);

	$("#control."+videoHTML.controlfirst+" span").addClass('active');	
	
	var submenu = $("#K_submenu").attr("class");					
	if (submenu=='allprograms')	{									
		var video = $("#K_selection").attr("class");				
		var exists = bookmarkCHECK(video);							
		if(exists)	{												
			$("#controls a span.bookmark").removeClass('active');	
			$("#controls a span.bookmark").addClass('working');		
			$("#controls a span.play").addClass('active');			
			$("#controls a.play").attr('rev','back');			
			$("#controls a.mainmenu").attr('rel','play');			
			$("#K_control").removeAttr("class");					
			$("#K_control").text('');								
			$("#K_controlfirst").removeAttr("class");				
			$("#K_controlfirgyyttst").text('');							
			$("#K_control").addClass('play');						
			$("#K_control").text('PLAY');							
			$("#K_controlfirst").addClass('play');					
			$("#K_controlfirst").text('PLAY');				
		}															
	}												
	
	$("#tertiaryLeft").show();
	$("#tertiaryRight").show();
	$("#tertiary").show();
	$(document).ready(function() {		
		$("#secondary").delay(200).fadeOut('fast');
	});
	
	return;
}

function gotoplaymovie()	{
	
	var selection = $("#carouselPosters img.current").attr("alt");
	
	if (!selection||selection==''||selection==' '||selection=='EMPTY3')	{	
		return;
	}
	
	$("#tertiary").removeAttr("class");
	$("#tertiary").addClass('movies');
	
	$("#K_selection").removeAttr("class");
	$("#K_selection").text('');
	// selection statement was here //
	$("#K_selection").addClass(selection);

	$("#K_panel").removeClass('secondary');
	$("#K_panel").addClass('tertiary');

	$("#K_control").removeAttr("class");
	$("#K_control").text('');
	$("#K_controlfirst").removeAttr("class");
	$("#K_controlfirst").text('');
	
	var menulabel = $("#K_menu").text();
	var submenulabel = $("#K_submenu").text();
	var submenu    = $("#K_submenu").attr("class");
	
	if(submenu == 'movies') {
		 $("#tertiaryTitle.menu").html('<p class="menu">&nbsp;</p>');	
	} else {
		$("#tertiaryTitle.menu").html('<p class="menu">'+menulabel+'</p>');	
	}
	$("#tertiaryTitle.submenu").html('<p class="submenu">'+submenulabel+'</p>');
	
	$("#K_movie").removeAttr("class");
	$("#K_movie").text('');

	var movieHTML = getMovieHTML();
	
	$("#K_movie").addClass(movieHTML.selection);
		
	$("#K_control").addClass(movieHTML.controlfirst);
	$("#K_control").text(movieHTML.controllabel);
	$("#K_controlfirst").addClass(movieHTML.controlfirst);
	$("#K_controlfirst").text(movieHTML.controllabel);
	
	$("#controls").html(movieHTML.controls);
	
	$("#controlimage").html(movieHTML.movie);
	$("#control."+movieHTML.controlfirst+" span").addClass('active');	

	$("#tertiaryLeft").show();
	$("#tertiaryRight").show();

	$("#secondary").hide();
	$("#tertiary").show();

	return;
}

function nextmovie(direction)	{
	
	var current = $("#K_movie").attr("class");
	$("#K_movie").removeAttr("class");
	
	if (direction=='LEFT')	{
		var next = $("#tertiaryRight #controlimage #movie p#next."+current).attr("title");
		$("#K_movie").addClass(next);
		if (next==current) return;
		$("#tertiaryRight #controlimage #movie."+current).hide();
		$("#tertiaryRight #controlimage #movie."+next).show();
		//$("#tertiaryRight #controlimage #movie."+next).show("slide", { direction: "right" }, 500);
	}	else	
		if (direction=='RIGHT')	{
			var prev = $("#tertiaryRight #controlimage #movie p#prev."+current).attr("title");
			$("#K_movie").addClass(prev);
			if (prev==current) return;
			$("#tertiaryRight #controlimage #movie."+current).hide();
			//$("#tertiaryRight #controlimage #movie."+prev).show("slide", { direction: "left" }, 500);
			$("#tertiaryRight #controlimage #movie."+prev).show();
		}
		
	return
}



function gotocontrols(control)	{

	var prevcontrol = $("#K_control").attr("class");

	$("#K_control").removeAttr("class");
	$("#K_control").text('');
	$("#control."+prevcontrol+" span").removeClass('active');

	$("#K_control").addClass(control);
	var controllabel = $("#control."+control+' span').text();
	$("#K_control").text(controllabel);	
	
	$("#control."+control+" span").addClass('active');
	
	return;
}

function gotocontrol()	{

	var currcontrol = $("#K_control").attr("class");
	var submenu 	= $("#K_submenu").attr("class");
	
	if (currcontrol=='play')	{
		
		$("#K_panel").removeClass('tertiary');
		$("#K_panel").addClass('video');
		$("#tertiaryLeft").hide();
		$("#tertiaryRight").hide();		
		$("#tertiary").hide();
	
		var version = $("#K_version").text();
		if (version=='TEST'||version=='ENSEO'||version=='TCM')	{
			if (submenu=='myprograms')
				var video = $("#K_choice").attr("class");
				else
				if (submenu=='allprograms')				
					var video = $("#K_selection").attr("class");
					else	
						var video = $("#K_movie").attr("class");
						
			var url = playVideo(video);
			if (submenu=='allprograms')	{								
				$("#controls a span.bookmark").addClass('working');	
				$("#controls a.play").attr('rev','mainmenu');		
				$("#controls a.mainmenu").attr('rel','play');		
				var choice = $("#K_choice").attr("class");				
				var choiceHTML = getSelections(choice);			
				var commentsHTML = getCommentsHTML();			
				$("#secondaryRight").html(choiceHTML.selections + commentsHTML.choicepanel);		
			}															
		}	
		return;		
		
	}	else
	if (currcontrol=='bookmark')	{
		
		var version = $("#K_version").text();
		if (version=='TEST'||version=='ENSEO'||version=='TCM')	{
			if (submenu=='myprograms')
				var video = $("#K_choice").attr("class");
					else	var video = $("#K_selection").attr("class");
			var bookmarks = bookmarkVideo(video);	
		}
		var choice = $("#K_choice").attr("class");
		var choiceHTML = getSelections(choice);		
		$("#secondaryRight").html(choiceHTML.choicelist);
		//var options = getLevelTwo(choice);
		var options = get(choice);
		
		buildselections(options); 
		$("#K_panel").removeClass('tertiary');
		$("#K_panel").addClass('secondary');
		$("#tertiary").hide();
		$("#secondary").show();
		
	}	else
	if (currcontrol=='remove')	{
	
		var version = $("#K_version").text();
		if (version=='TEST'||version=='ENSEO'||version=='TCM')	{
			var video = $("#K_choice").attr("class");
			var bookmark = cancelVideo(video);
		}		

		$("#K_panel").removeClass('tertiary');
		$("#tertiaryLeft").hide();
		$("#tertiaryRight").hide();		
		$("#tertiary").hide();
		gotosecondary();
		
	}	else	
	if (currcontrol=='back')	{
	
		$("#K_panel").removeClass('tertiary');
		$("#K_panel").addClass('secondary');
		//$("#tertiary").fadeOut('fast');
		//$("#secondary").fadeIn('fast');
		$("#tertiary").hide();
		$("#secondary").show();
		
	}	else
	if (currcontrol=='mainmenu')	{
		var background = $("#K_panel").text();
		if (background)
		$("#secondary").removeClass(background);
		$("#K_panel").removeClass('tertiary');
		$("#K_panel").addClass('primary');
		$("#K_panel").text('');
		$("#K_subpanel").removeAttr('class');
		$("#K_subpanel").text('');
		$("#K_choice").removeAttr('class');
		$("#K_choice").text('');
		$("#K_selection").removeAttr('class');
		$("#K_selection").text('');
		$("#K_control").removeAttr('class');
		$("#K_control").text('');
		$("#K_choicefirst").removeAttr('class');
		$("#K_choicefirst").text('');
		$("#K_selectionfirst").removeAttr('class');
		$("#K_selectionfirst").text('');
		$("#K_controlfirst").removeAttr('class');
		$("#K_controlfirst").text('');
		$("#secondary div#secondaryLeft div#choices").text('');
		$("#secondary div#secondaryRight").text('');
		//$("#tertiary").fadeOut('fast');
		//$("#primary").fadeIn('fast');
		$("#tertiary").hide();
		$("#primary").show();		
		
	}
	
	return;
}

function buildselections(options)	{
	msg('in buildselections ' + options);
	$("#secondary.allprograms div#secondaryLeft div#choices a span.active").css("color","#344e9b");

	$("#K_subpanel").addClass('selections');
	
	$("#K_selection").removeAttr("class");
	$("#K_selection").text('');
	$("#K_selectionfirst").removeAttr("class");
	$("#K_selectionfirst").text('');	

	var selectionsHTML = getSelectionsHTML(options.panel);
	
	$("#K_selection").addClass(selectionsHTML.selectionfirst);
	$("#K_selection").text(selectionsHTML.selectionlabel);
	$("#K_selectionfirst").addClass(selectionsHTML.selectionfirst);
	$("#K_selectionfirst").text(selectionsHTML.selectionlabel);	
	
	//$("#secondaryRight #choicepanel4").html(selectionsHTML.selections);		
	$("#secondaryRight #choicepanel #choicepanel4 #selection."+selectionsHTML.selectionfirst+" span").addClass('active');
	//$("#secondaryRight #choicepanel #choicepanel1 #selection."+selectionsHTML.selectionfirst+" p").addClass('active');
	//$("#choicepanel3").html('Press SELECT to Bookmark or Watch');

	//$("#secondaryRight #choicepanel4").show("slide", { direction: "up" }, 300);
	$("#secondaryRight #choicepanel4").show();
	
	return;
}

// myprograms, allprograms selections 

function gotoselections(selection)	{
	
	var prevselection = $("#K_selection").attr("class");

	$("#K_selection").removeAttr("class");
	$("#K_selection").text('');
	$("#selection."+prevselection+" span").removeClass('active');
	
	$("#K_selection").addClass(selection);
	$("#K_selection").addClass(selection);
	var selectionlabel = $("#selection."+selection+" span").text();
	
	$("#K_selection").text(selectionlabel);	
	
	$("#selection."+selection+" span").addClass('active');
	
	return;
}

function gotoselection(selection)	{

	if (selection=='more')	{
	
		moreselections();
		
	}	else
		if (selection=='back')	{	

			removeselections();
			
		}	else	{
			
				var options = getLevelTwo(selection);
				
				if (options.tag=='EMPTY1'||options.tag=='EMPTY2'||options.tag=='EMPTY3')	{	
					return;
				}
				
				if (options.type=='video')	{
					gotoplayvideo(options);
				}	else
					if (options.type=='movie')	{
						gotoplaymovie();			
					}	else	{
							msg('gotoselection: '+selection+': '+options.type+' not supported');
							return false;
						}	
		
			}
	
	return;
}

function moreselections(key)	{

	var selectionlast = $("#K_selection").attr("class");
	$("#K_selection").removeAttr("class");
	$("#K_selection").text('');
	$("#K_selectionfirst").removeAttr("class");
	$("#K_selectionfirst").text('');
	var selection = $("#K_choice").attr("class");
	//var selectionlast = $("#selection.more").attr("rev");

	var selectionsHTML = getSelectionsHTML(selection,selectionlast,key);
	var commentsHTML = getCommentsHTML();					
	$("#secondaryRight").html(selectionsHTML.selections + commentsHTML.choicepanel);		
	
	$("#K_selection").addClass(selectionsHTML.selectionfirst);
	$("#K_selection").text(selectionsHTML.selectionlabel);
	$("#K_selectionfirst").addClass(selectionsHTML.selectionfirst);
	$("#K_selectionfirst").text(selectionsHTML.selectionlabel);	
	
	//$("#secondaryRight #choicepanel1").html(selectionsHTML.choicelist);
	
	//$("#secondaryRight #choicepanel4").html(selectionsHTML.selections);
	$("#secondaryRight #choicepanel #choicepanel4 #selection."+selectionsHTML.selectionfirst+" span").addClass('active');
	//$("#secondaryRight #choicepanel #choicepanel1 #selection."+selectionsHTML.selectionfirst+" p").addClass('active');
	//$("#choicepanel3").html('Press SELECT to Bookmark or Watch');
	
	//$("#secondaryRight #choicepanel4").show("slide", { direction: "up" }, 300);
	$("#secondaryRight #choicepanel4").show();
	
	return;
}

function removeselections()	{
	
	$("#secondary.allprograms div#secondaryLeft div#choices a span.active").css("color","#ffffff");
	
	$("#K_subpanel").removeAttr("class");
	
	$("#K_selection").removeAttr("class");
	$("#K_selection").text('');
	$("#K_selectionfirst").removeAttr("class");
	$("#K_selectionfirst").text('');
	gotomainmeu();
}


function gotomainmenu() {
		
		document.getElementById("secondary").style.zIndex=700;
		var indexHTML = initIndexHTML();
		$("#kookies").html(indexHTML.kookies);
		$("#secondary").html(indexHTML.secondary);
		$("#tertiary").html(indexHTML.tertiary);
		$("#watchtv").html(indexHTML.watchtv);
		
		var tag = $("#secondary").attr("class");
		$("#secondary").removeClass(tag);
		tag = $("#tertiary").attr("class");
		$("#tertiary").removeClass(tag);
		tag = $("#K_panel").attr("class");
		$("#K_panel").removeClass(tag);
		tag = $("#K_subpanel").attr("class");
		$("#K_subpanel").removeClass(tag);
		
		$("#K_panel").addClass('primary');
		var tag = $("#K_subpanel").attr("class");			
		$("#K_subpanel").removeClass(tag);
		$("#K_subpanel").text('');
								
		var tag = $("#K_panel").attr("class");				
		$("#K_panel").removeClass(tag);		
		$("#K_panel").addClass('primary');
					
		$("#secondary div#cover").hide();
		$("#tertiary").hide();
		$("#secondary").hide();
		$("#pictures").hide();		
		$("#images").hide();		
		
		$("#primary").show();						
				
		var menu = $("#K_menufirst").attr("class");
		
		gotomenu(menu);
		return true;
		
	}
	
