// !! Notice
// In order for the config webservice to work, we need a host item in HSDB hosts file
// x.x.x.x	ACESOBOOTSERVER
// where the x.x.x.x is the client-2-server IP of the integration server
if(typeof ewfObject != 'function'){
	window.ewfObject = function(){
		var ewf = getPortalConfig();
		return ewf;
	};
}

// Get portal configuration items.
// This function will request a dynamic Ajax call to integration server to get config data.
function getPortalConfig(forceUpdate) {
	
	var prov = getprovisionData();
	var portalID = prov['portalID'];
	
	var webservice_url = '/Proxy/ServerProxy?wsdl=http://ACESOBOOTSERVER:9080/ams/aceso/getPortalConfigJson?portal=' + portalID;
	
	if(window['CONFIG_DATA'] && !forceUpdate) {
		// if configuration data already exists
		return window.CONFIG_DATA;
	}
	
	window['CONFIG_DATA'] = {};
	
	// try web service
	$.ajax({
	    type: 'GET',
	    url: webservice_url,
	    dataType: 'json',
	    async: false,
	    success: function(data) {
	    	if(!data.response)
	    		return;
	    	window['CONFIG_DATA'] = data.response;
	    	var client = data.response.client;
			$("#K_client").text(client);
	    },
	    error: function() {
	    	$("#loadinginfo").append('<p>Failed to get config data</p>')
	    }
	});
	
	return window['CONFIG_DATA'];
}

// Get basic device provision data.
// This function will return cached data whenever available, unless 'forceUpdate' is true.
// Note this function only returns generic device related data.  No patient data or portal dedicated manipulation is involved.
// Sample output:
// {deviceID:"0050C25C",nodegroupID:"4",deviceName:"Guest",homeID:"1001_a",roomNumber:"1001",roomBed"a",mode:"1",state:"1",status:"0",stbID:"0050C25C",TVNumber:"B",TVInputMode:"Input 5",siteID:"100"}
function getprovisionData(forceUpdate) {

	forceUpdate = typeof forceUpdate !== 'undefined' ? forceUpdate : false;
	
	if(window['PROVISION_DATA'] && !forceUpdate) {
		// if provision data already exists
		return window.PROVISION_DATA;
	}
	
	var mac		= getDevice();
	
	var arg1	= 'attribute=json_libs_oss_get_user_data';
	var arg2	= 'device_id='+mac;
	
	var url		= '/stbservlet';
	var args    = arg1+'&'+arg2;
	var userData = ajaxJSON(url,args);
	
	var provisionData = {};
	
	if(userData.DataArea && userData.DataArea.length>0) {
		var dataItem = userData.DataArea[0];
		
		//copy everything from device section, and split homeID to get room and bed.
		if(dataItem.ListOfDevice && dataItem.ListOfDevice.length>0) {
			
			var device = dataItem.ListOfDevice[0];
			provisionData = device.tagAttribute;
			
			if(provisionData.homeID) {
				var pos = provisionData.homeID.indexOf("_");
				if(pos>=0) {						
					provisionData['roomNumber'] = provisionData.homeID.substr(0,pos);
					provisionData['roomBed'] = provisionData.homeID.substr(pos+1);
				}
			}
		}
		
		//add portalID into result.
		if(dataItem.tagAttribute && dataItem.tagAttribute.portalID) {
			provisionData['portalID'] = dataItem.tagAttribute.portalID;
		}
	}
	
	window['PROVISION_DATA'] = provisionData;
	return window.PROVISION_DATA;
}
