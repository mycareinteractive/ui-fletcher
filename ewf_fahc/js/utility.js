/********************************************************************************
 * @brief																		*
 * 		Utility Functions 														*	
 *			DateTime,Msg,Dec2Hex,GetHour,CheckforReload,RoundNumber,checkobj,   *
 *			Gets and Valides Program Data - EPG						            *
 *																				*
 * @author																		*
 *		Bill Sears\n															*
 *		Aceso\n																	*
 *		http://www.aceso.com\n													*
 *																				*
 * @modified																	*
 * 		Tami Seago, 02/05/2013, add info and error handling						*
 *																				*
 *		Bernie Zhao, 03/06/2013, added logging to browser console   			*
 ********************************************************************************/

function datetime()	{
//create the a Formated Date Time Field

	var d 		= new Date();
	var dow 	= d.getDay();
	var hour 	= d.getHours();
	var minute	= d.getMinutes();
	var month	= d.getMonth();
	var day		= d.getDate();
	var year 	= d.getFullYear();
	var ampm	= 'AM';
	if (day<10) { 
	day = '0' + day;
	}

	if (hour>12)	{
		hour = hour-12;
		ampm  = 'PM';
	}
	if (minute<10)	{
		minute = '0'+minute;
	}	
	

	var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
	var months  = ["01","02","03","04","05","06","07","08","09","10","11","12"];
			
	var datetime = '<p>'+weekday[dow]+', '+months[month]+'.'+day+'.'+year+', '+hour+':'+minute+' '+ampm+'</p>';
	
	$("#datetime").html(datetime);
	
	return;
}


function msg(message)	{
	//used for logging messages
	
	var version = $("#K_version").text();
	var client  = $("#K_client").text();
	
	if (version=='TEST')	{
		//alert(message);
		if(typeof window.console != 'undefined')
			console.log(message);
	}	else
	if (version=='ENSEO')	{
		Nimbus.logMessage('|'+client+'| '+message );
	} else
	if (version=='TCM')	{
//		alert(message);
	}	

	
}

function dec2hex(id){

	var returns = '';
	var cha = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
	var temp = '';
	while (id > 0){
		i = id % 16;
		id = Math.floor(id / 16);
		temp = cha[i] + temp;
	}
	returns = temp.toLowerCase();
	
	return returns;
}

function getHour()	{

	var d 		= new Date();
	var hour 	= d.getHours();

	return hour;
}

function checkforReload()	{
	
	var reloadtime = $("#K_reload").text();
	msg('checkforReload...reloadtime: '+reloadtime);
	
	if (reloadtime=='OFF')
		return;
		
	var trigger = reloadtime*1;
	var hour = getHour();
	msg('checkforReload...hour/trigger: '+hour+'/'+trigger);
	
	if (hour!=trigger)
		return;
	
	var power = checkPower();
	msg('checkforReload...power: '+power);
	
	if (power=='OFF')	{
		
		msg('checkforReload...RELOAD');
		
		var panel = $("#K_panel").attr("class");

		if (panel=='video'||panel=='scenictv')	stopVideo();
		if (panel=='tv')	stopTV();
		
		reloadapp();
	}
}

function roundNumber(num,dec) {
	
	var result = num;
	
	if (num!=0)
		result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	
	if (dec==2)	{
		var resultS = result.toString();
		var resultL = resultS.length;
		var dp 		= resultS.indexOf('.');
		if(dp==-1)	{
			result = resultS+'.00';	
		}	else	{
				var diff = (resultL-1) - dp;
				if (diff==1)
					result = resultS+'0';
			}
	}
	
	return result;
}

function checkObj(object) {

var stringConstructor = "test".constructor;
var arrayConstructor = [].constructor;
var objectConstructor = {}.constructor;

    if (object === null) {
        return "null";
    }
    else if (object === undefined) {
        return "undefined";
    }
    else if (object.constructor === stringConstructor) {
        return "string";
    }
    else if (object.constructor === arrayConstructor) {
        return "array";
    }
    else if (object.constructor === objectConstructor) {
        return "object";
    }
    else {
        return "don't know";
    }

}

function resetIdleTimer(seconds) {
	var ewf = ewfObject();
	var ms = ewf.killTreeTime*1000;
	if(seconds)
		ms = seconds*1000;
		
	window.clearTimeout(window.idle_timeout_handle);
	window.idle_timeout_handle = setTimeout(function(){
		var version = $("#K_version").text();
		if (version=='TCM'||version=='TEST')	{
			killApplication();
		}
	}, ms);
}