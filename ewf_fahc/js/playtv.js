/********************************************************************************
 * @brief																		*
 * 		Play TV Functions					               						*
 *																				*
 * @author																		*
 *		Bill Sears\n															*
 *		Aceso\n																	*
 *		http://www.aceso.com\n													*
 *																				*
 * @modified																	*
 * 		Tami Seago, 02/05/2013, add info and error handling						*
 ********************************************************************************/

function playTV(channel)	{
	
	var grid = $("#K_grid").attr("class");
		
	var ewf = ewfObject();
	var url = '';
	
	var channels 	  = epgChannels();
	var programNumber = '';
	var frequency 	  = '';
	var channelNumber = '';
	var channelName   = '';
	var scenictvF     = '57000';
	var channelUsage  = '';
	var version = $("#K_version").text();
msg(version);	
	
	$.each(channels['Channels'], function(i,row){	
										  
		if (channel==row["channelID"])	{
			
			$("#VIDEO_nowplaying").text(channel);
			
			programNumber = row["programNumber"];
			frequency	  = row["frequency"];
			channelNumber = row["channelNumber"];
			channelName   = row["channelName"];
			
			if (row["channelUsage"])
				channelUsage = ' ChannelUsage="'+row["channelUsage"]+'"';
			
			var overlay = '<p>'+channelNumber+' '+channelName+'</p>';
			$("#overlay #channelinfo").html(overlay);
			$("#overlay").fadeIn('slow');
			$("#overlay #channelinfo").fadeIn('slow');
			
		}		
	});	
		
	if (version == 'ENSEO') {
	var parm1 = 'PhysicalChannelIDType=Freq';
	var parm2 = 'PhysicalChannelID="'+frequency+'"';
	var parm3 = 'DemodMode="QAM'+ewf.EPGChannelQAMMode+'"';
	var parm4 = 'ProgramSelectionMode="'+ewf.EPGProgramSelectionMode+'"';
	var parm5 = 'ProgramID="'+programNumber+'"';

		var parms = ' '+parm1+' '+parm2+' ';
		url = '<ChannelParams ChannelType="Analog"'+channelUsage+'><AnalogChannelParams'+parms+'></AnalogChannelParams></ChannelParams>';
	var sessionID = getSessionID();
	
		var player = Nimbus.getPlayer( url, null, sessionID );
		if (player)	{
				
			player.setChromaKeyColor(0x00000000);
			player.setVideoLayerBlendingMode("colorkey");
			player.setVideoLayerTransparency(1);
			player.setPictureFormat("Widescreen");
			player.setVideoLayerRect(0, 0, 1280, 720);
			player.setVideoLayerEnable(true);
		
			player.play();
		}
				
	} 	else
	if (version=='TCM' || version=='TEST') {
		var mac	= getDevice();
		var url = 'http://'+ewf.pollingIP+':'+ewf.pollingPort+'/pollservlet?attribute=JumpToChannel&deviceID='+mac+ '&ChannelNumber=' + channelNumber;
		var callbackString = ajaxXML('STREAM',url);
		msg(callbackString);
		if (callbackString.indexOf("true") >= 0) {
			var SS_streaming = true;
		} else {
			var SS_streaming = false;
		}
		}		
		
		killApplication();	
	
	$("#overlay #channelinfo").delay(3000).fadeOut('slow');
	
	return url;
}

function getSessionID()	{

	var mac	= getDevice();
	var sessionID = 'SEC'+mac.substr(7,5);

	var d = new Date();
	var day		= d.getDate();
	var month	= d.getMonth() + 1;	
	var hours 	= d.getHours();
	var minutes	= d.getMinutes();
	var seconds	= d.getSeconds();

	var year 	= d.getFullYear() - 2000;
	
	day 	= fixZero(day);
	month 	= fixZero(month);
	hours 	= fixZero(hours);
	minutes = fixZero(minutes);
	seconds = fixZero(seconds);
	
	var sessionDate = day+month+year+hours+minutes+seconds;
	
	sessionID = sessionID+sessionDate;
	
	return sessionID;	
}

function donothing()	{
	return;
}

function fixZero(val)	{
	val = ''+val;
	if (val.length==1) 
		val = '0'+val;
	return val;
}

function scenicTV()	{
	
	var ewf = ewfObject();
	
	var url = playVideo(ewf.scenictvVideo);
	return url;
}

function updnChannel(key)	{
	
	var channels = epgChannels();
	var channel  = $("#VIDEO_nowplaying").text();
	var curr     = '';
	var prev     = '';
	var next     = '';
	var first	 = '';
	var last	 = '';
	
	$.each(channels['Channels'], function(i,row){
		if (first=='') 
			first = row["channelID"];
		if (curr!=''&&next=='')   	
			next = row["channelID"];
		last = row["channelID"];
		if (channel==row["channelID"])	{
			curr = row["channelID"];
		}
		if (curr=='')
			prev = row["channelID"];
	});
	if (prev=='') prev = last;
	if (next=='') next = first;
	
	if (key=='CHUP') channel = next;
		else
		if (key=='CHDN') channel = prev;
		
	var stoptv = stopTV();
	var playtv = playTV(channel);	
	
	return playtv;
}

function keyChannel(key)	{
	
	
	var cLength = 1;
	
	var keycheck = $("#K_keycheck").text();
	if (keycheck)	{
		clearInterval(keycheck);
		$("#K_keycheck").text('');
	}
	
	var pKey = $("#K_key").text();
	
	if (pKey)	{
		$("#K_key").text('');
		pKey = pKey+key;
		cLength = pKey.length;
		if 	(cLength == 3)	{
			key3Channel(pKey);
			return;
		}
		key = pKey;
	}
	
	if (cLength==1&&key=='0')	
		return;
	
	$("#K_key").text(key);
	var keycheck = setInterval( "key2Channel()", 2000 );
	$("#K_keycheck").text(keycheck);
	
	
	return;
}

function key2Channel()	{
	
	
	var keycheck = $("#K_keycheck").text();
	clearInterval(keycheck);
	$("#K_keycheck").text('');
	
	var pKey = $("#K_key").text();	
	$("#K_key").text('');
	
	key3Channel(pKey);	
}

function key3Channel(pKey)	{
	
	//msg('key3Channel');

	var channels 	  = epgChannels();
	var channel		  = '';
	
	$.each(channels['Channels'], function(i,row){	
										  
		if (pKey==row["channelNumber"])	{
			channel = row["channelID"];
			//msg('channel: '+pKey+' '+channel);
		}
		
	});	
	
	
	if (channel)	{
		stopTV();
		playTV(channel);
	}	else	{
			var overlay = '<p>'+pKey+' not available</p>';
			$("#overlay #channelinfo").html(overlay);
			$("#overlay").fadeIn('slow');
			$("#overlay #channelinfo").fadeIn('slow').delay(3000).fadeOut('slow');	
		}
	
	return;
}

function stopTV()	{
	
	$("#overlay #channelinfo").hide();
	$("#overlay").hide();
	
	var version = $("#K_version").text();
	if (version=='ENSEO')	{
		var player = Nimbus.getPlayer();
		if (player)	{
			player.setVideoLayerEnable(false);
			player.stop();
			player.close();
		}	
	}
	
	return;	
}

function setCC()	{
	
	var version = $("#K_version").text();
	if (version=='ENSEO')	{
		var cc = Nimbus.getCCMode();
		if (cc=='Off')	{
			Nimbus.setCCMode('On');
			msg('Closed Captioning has been turned ON');
		}	else
			if (cc=='On')	{
				Nimbus.setCCMode('Off');
				msg('Closed Captioning has been turned OFF');
			}
	}
	
	return;	
}

