/********************************************************************************
 * @brief																		*
 * 		Common JSON call functions			               						*
 *																				*
 * @author																		*
 *		Bill Sears\n															*
 *		Aceso\n																	*
 *		http://www.aceso.com\n													*
 *																				*
 * @modified																	*
 * 		Tami Seago, 02/05/2013, add info and error handling						*
 *																				*
 *		Bernie Zhao, 03/06/2013, added support for Live555 RTSP server			*
 *         replaced all absolute path config with relative path                 *
 ********************************************************************************/

function ewfObject()	{
	var ewf = {};
		ewf = {
			"client" : 'FAHC'
		};
		 client = ewf.client;
		 $("#K_client").text(client);
	
	if (client=='FAHC') {
		 ewf = {
			"host":	"/",
			"method":"stbservlet",
			"menuhost":"http://192.168.100.10:9080/ams/aceso/getMenuXml?portal=fahc",
			"proxy":"Proxy/ServerProxy",
			"pollingIP": "192.168.100.50",
			"pollingPort": "554",			
			"applicationUID":"60010001",
			"nodeGroup":"1",
			"VODRootHUID":"HTNN3",
			"regionChannelGroup":"1",
			"movies":"HTNN6",
			"allprograms":"HTNN9",		
			"RTSPTransport_Enseo":"MP2T/AVP/UDP",
			"RTSPTransport_TCM":"MP2T/DVBC/QAM",
			"ServiceGroup_ENSEO":"2",
			"ServiceGroup_TCM":"1",	
			"ServiceGroup":"2",
			"Poster": "/Poster/",
			"epgFilePath": "../AcesoTribuneEPG/FULLEPG/",
			"EPGChannelType": "0", 	
			"EPGChannelFrequency": "375000", 
			"EPGChannelProgramNumber": "3", 
			"EPGChannelQAMMode": "256",
			"EPGProgramSelectionMode": "PATProgram",
			"scenictvChannel":"2",
			"scenictvVideo":"A1000000080",
			"scenictvUID":"1000000080",
			"scenictvPOUID":"7",
			"ewflocation":"/index.html", 
			"blackbox":"http://192.168.100.10",
			"VODServerType":"SeaChange",
			"killTreeTime":"120"
			};
	}
	
	return ewf;
}

function getRating(code)	{
	
	var rating  	= '';
	var ratingA 	= Array();
	
	ratingA['0'] = 'not rated';
	ratingA['1'] = 'G';
	ratingA['2'] = 'PG';
	ratingA['3'] = 'PG-13';
	ratingA['4'] = 'R';
	ratingA['5'] = 'NC-17';
	ratingA['6'] = 'Adult';
	ratingA['7'] = 'unknown';
	ratingA['8'] = 'unknown';
	ratingA['9'] = 'unknown';
	
	rating = ratingA[code-1];
	
	return rating;
}

function getDevice() {
	
	var currentURL = location.href;	
	var version = $("#K_version").text();
	var mac = $("#K_mac").text();
	
	if ( typeof window['EONimbus'] != 'undefined' ) {			
		var device_list = Nimbus.getNetworkDeviceList();
		var username = device_list[0];
		var device = Nimbus.getNetworkDevice( username );
		var mac = device.getMAC().replace( /:/gi, '' );
		version = 'ENSEO';		
	
	} else 
	
	if ( typeof window['EONebula'] != 'undefined' ) {			

		var device_list = Nebula.getNetworkDeviceList();
        for (var id=0; id<devList.length; id++)
        {
            var dev = Nebula.getNetworkDevice(devList[id]);
			var mac = dev.getMAC();
        }
		version = 'NEBULA';		
		
	} else {		
			if(!version||!mac) {
			if ( currentURL.indexOf("https:\/\/") != -1 ) var obj = currentURL.replace("https:\/\/", "");
			else var obj = currentURL.replace("http:\/\/", "");
			obj=obj.split("\/");
			var currentDomain = obj[0];			
			var currentPortal = obj[1];			
			paramArray = obj[2].replace("index.html?","").split("\&");		
			var i;			
			for(var i = 0;i<paramArray.length;i++){
				if(unescape(paramArray[i].split("\=")[0])=="device-id") var mac = paramArray[i].split("\=")[1];
				if( unescape(paramArray[i].split("\=")[0])=="PollingIP")  var PollingIP = paramArray[i].split("\=")[1]||"";if( unescape(paramArray[i].split("\=")[0])=="PollingPort")  var PollingPort = paramArray[i].split("\=")[1]||"";
				if( unescape(paramArray[i].split("\=")[0])=="pageName")  var pageName = paramArray[i].split("\=")[1]||"";
				if( unescape(paramArray[i].split("\=")[0])=="assetLocalEntryUID")   var assetLocalEntryUID = paramArray[i].split("\=")[1]||"";
				if( unescape(paramArray[i].split("\=")[0])=="ticketID")  var ticketID = paramArray[i].split("\=")[1]||"";
				if( unescape(paramArray[i].split("\=")[0])=="guestName")  var guestName = paramArray[i].split("\=")[1]||"";				
			}				
			version = 'TCM';
			}
	}
	
	if (!mac||mac=='test')	{
		mac = "09031462";				
		version = 'TEST'
	} else
		if(mac=='test2') {
			mac = "0016E87DDA62";				
			version = 'TEST'
		} 
	 $("#K_mac").text(mac);	
	 $("#K_version").text(version);	
	return mac;
}

function cleanJSON(data)	{
	
	data = clean2JSON(data);
	data = clean5JSON(data);

	return data;
}

function clean1JSON(data)	{

	check1 = data.indexOf('/');
	if (check1 != -1)	{
		while (data != (data = data.replace('/','&#47;')));
	}
	
	return data;
}

function clean2JSON(data)	{

	check2 = data.indexOf('\\');
	if (check2 != -1)	{
		while (data != (data = data.replace('\\','&#47;')));
	}
	
	return data;
}

function clean3JSON(data)	{

	check3 = data.indexOf(',http');
	if (check3 != -1)	{
		while (data != (data = data.replace(',http',' http')));
	}
	
	return data;
}

function clean4JSON(data)	{

	check4 = data.indexOf('http://192.168.200.60:9090/Poster/');
	if (check4 != -1)	{
		while (data != (data = data.replace('http://192.168.200.60:9090/Poster/','')));
	}	
	check4 = data.indexOf('http://192.168.100.60:9090/PICTURE/FOLDER/');
	if (check4 != -1)	{
		while (data != (data = data.replace('http://192.168.100.60:9090/PICTURE/FOLDER/','')));
		
	}	
	return data;
}

function clean5JSON(data)	{

	while (data != (data = data.replace(/[\r\n]/g, '')));
	
	return data;
}

function clean6JSON(data)	{

	while (data != (data = data.replace(null+',', '')));
	
	return data;
}


function ajaxJSON(url,args,ignore)	{
	
	$("#CALL_current").html(url+'?'+args);
	try {
	var data = $.ajax({
		url: url,
		data: args,
		async: false,
		dataType: "json"
		}).responseText;
	
	if (ignore) 						{ msg('Ignoring JSON Returned Data: '+url+'?'+args+'  resulted in: '+data); return; };
	if (!data) 							{ msg('FATAL ERROR!! JSON Data Not Found:  '+url+'?'+args+'  resulted in: NO DATA'); return; };
	if (data==''||data==' ') 			{ msg('FATAL ERROR!! JSON Data Incomplete: '+url+'?'+args+'  resulted in: '+data); return; };
	if (data.indexOf('{0}') != -1) 		{ msg('FATAL ERROR!! JSON Data Incomplete: '+url+'?'+args+'  resulted in: '+data); return; };
	if (data.indexOf('<?xml') != -1)	{ msg('FATAL ERROR!! JSON Data is XML: '+url+'?'+args+'  resulted in: '+data); return; };
	if (data.indexOf('<html>') != -1)	{ msg('FATAL ERROR!! JSON Data is HTML: '+url+'?'+args+'  resulted in: '+data); return; };	 
	if (data.indexOf('errorCode') != -1)	{ //msg('FATAL ERROR!! Error Code Received in JSON: '+url+'?'+args+'  resulted in: '+data); 
											gotomainmenu();	};	 
	
	}
	catch (e) {
		msg(e.message);
		gotomainmenu();		
	}
	
	data = cleanJSON(data);

	$("#EWF_current").html(data);

	var jsondata = eval('(' + data + ')');
	
	return jsondata;
}

function loadEWF(type)	{

	var jsonString = $("#EWF_"+type).text();
	var jsonObject = JSON.parse(jsonString);

	return jsonObject;
}

function loadJSON(type)	{

	var jsonString = $("#JSON_"+type).text();
	jsonString = cleanJSON(jsonString);	
	var jsonObject = JSON.parse(jsonString);

	return jsonObject;
}

function saveJSON(type,jsonObject)	{

	var jsonString = JSON.stringify(jsonObject);
	$("#X_"+type).text(jsonString);
	
	return true;
}

function getJSON(type)	{

	var jsonString = $("#X_"+type).text();
	jsonString = cleanJSON(jsonString);		
	var jsonObject = JSON.parse(jsonString);

	return jsonObject;
}
// CHANGE BEG: 20121024 sthummala - change via Tami - convert menu xml to js/json object
function getJSONMenu(type)	{
	var jsonString = $("#X_JSON_menu").text();
	jsonString = clean6JSON(jsonString);
	
	var jsonObject = JSON.parse(jsonString);
	return jsonObject;
}

function XML2jsobj(node) {

	var	data = {};

	// append a value
	function Add(name, value) {	
		if (data[name]) {
			if (data[name].constructor != Array) {
				data[name] = [data[name]];
			}
			data[name][data[name].length] = value;
		}
		else {
			data[name] = value;
		}
	};
	
	// element attributes
	var c, cn;
	/* Shirisha Thummala - Since no attributes commented the below part of the code */
	//for (c = 0; cn = node.attributes[c]; c++) {
	//	Add(cn.name, cn.value);
	//}
	
	// child elements
	for (c = 0; cn = node.childNodes[c]; c++) {
		if (cn.nodeType == 1) {
			if (cn.childNodes.length == 1 && cn.firstChild.nodeType == 3) {
				// text value
				Add(cn.nodeName, cn.firstChild.nodeValue);
			}
			else {
				// sub-object
				Add(cn.nodeName, XML2jsobj(cn));
			}
		}
	}
	
	return data;
	
}
// CHANGE END: 20121024 sthummala - change via Tami - convert menu xml to js/json object