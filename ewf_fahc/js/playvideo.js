/********************************************************************************
 * @brief																		*
 * 		Play Video Functions				               						*
 *																				*
 * @author																		*
 *		Bill Sears\n															*
 *		Aceso\n																	*
 *		http://www.aceso.com\n													*
 *																				*
 * @modified																	*
 * 		Tami Seago, 02/05/2013, add info and error handling						*
 *																				*
 *		Bernie Zhao, 03/06/2013, added support for Live555 RTSP server			*
 ********************************************************************************/

function bookmarkVideo(video)	{
	
	$("#controls a span.bookmark").addClass('working');
	
	var bookmarks = getBOOKMARKS();
	msg('bookmarks ' + bookmarks);
	var exists    = '';
	$.each(bookmarks['myprograms'], function(i,row){									
		if (video==row["tag"])	{
			exists = row["tag"];
		}
	});
	if (exists!='')		return bookmarks;
	
	var selection 		= getSELECTION(video);
	var purchaseDATA 	= purchaseAsset(selection.poUID,selection.tag);	// purchase the movie/video
	msg(purchaseDATA);	
	var myprogramsDATA 	= getmyprogramsDATA();

	var allprogramsDATA = getAllProgramsDATA();
	
	bookmarks = getBOOKMARKS();
	
	return bookmarks;
}

function playVideo(video)	{

	var ewf 		= ewfObject();
	var bookmarks 	= bookmarkVideo(video);
	var serverload  = getServerLoadDATA();
	var deviceID	= '';
	var homeID		= '';
	var assetID		= '';
	var ticketID	= '';
	var position	= 0;
	
	$.each(bookmarks['myprograms'], function(i,row){
		if (video==row["tag"])	{
			deviceID	= row["deviceID"];
			homeID		= row["homeID"];
			assetID		= row["assetID"];
			ticketID	= row["ticketID"];
			if (row["position"])	{
				var positionA = row["position"].split('.');
				position = positionA[0];
			}			
		}
	});	

	//msg(deviceID);

	$("#loading").show();
	
	$("#primary").hide();

	var version = $("#K_version").text();
msg(version);
	//	version = "ENSEO";
	if(version=="TCM"||version=="TEST") {
	
		var assetID = dec2hex(assetID);
	
		var parm1		= '60010001?assetUid='+assetID;	
		var parm2		= 'transport='+ewf.RTSPTransport_TCM;
		var parm3		= 'ServiceGroup='+ewf.ServiceGroup_TCM;
		var parm4		= 'smartcard-id='+deviceID;
		var parm5		= 'device-id='+deviceID;
		var parm6		= 'home-id='+homeID;
		var parm7		= 'purchase-id='+ticketID;
	
		var parms		= parm1+'&'+parm2+'&'+parm3+'&'+parm4+'&'+parm5+'&'+parm6+'&'+parm7;
					var url = 'http://' + ewf.pollingIP + ':' + ewf.pollingPort;
		
		url += "/pollservlet?attribute=SetupStream&ServerIP=";
		url += serverload.IPAddress;
		url += "&ServerPort=";
		url += serverload.Port;
		url += "&DeviceId=";
		url += deviceID;
		url += "&PlayPosition=";
		url += position;
		url += "&URL=";
		url += escape(parms);
				
		msg('url in playvideo ' + url);			
	
		var callbackString = ajaxXML('STREAM',url);
	msg('callbackstring ' + callbackString + ' ' + callbackString.indexOf("false"));
		if (callbackString.indexOf("false") >= 0) {
			var SS_streaming = false;
		} else {
			var SS_streaming = true;
		}
		killApplication();			
	}
	
	if (version=='ENSEO')	{	
		
		var url = '';
		if(typeof ewf.VODServerType != 'undefined') {
			if(ewf.VODServerType == 'Live555') {
				url = '<ChannelParams ChannelType="RTSP"><RTSPChannelParams URL="rtsp://'+serverload.IPAddress+':'+serverload.Port+'/'+assetID+'.ts"></RTSPChannelParams></ChannelParams>';
			}
		} else {f
			var assetID = dec2hex(assetID);
			var parm1		= 'assetUid='+assetID;	
			var parm2		= 'transport='+ewf.RTSPTransport_Enseo;
			var parm3		= 'ServiceGroup='+ewf.ServiceGroup_ENSEO;
			var parm4		= 'smartcard-id='+deviceID;
			var parm5		= 'device-id='+deviceID;
			var parm6		= 'home-id='+homeID;
			var parm7		= 'client_mac='+deviceID;
			var parm8		= 'purchase-id='+ticketID;
		
			var parms		= parm1+'&amp;'+parm2+'&amp;'+parm3+'&amp;'+parm4+'&amp;'+parm5+'&amp;'+parm6+'&amp;'+parm7+'&amp;'+parm8;
		

			var url = '<ChannelParams ChannelType="RTSP" Encryption="Proidiom"><RTSPChannelParams URL="rtsp://'+serverload.IPAddress+':'+serverload.Port+'/'+ewf.applicationUID+'" OpenParams="'+parms+'"></RTSPChannelParams></ChannelParams>';
		}

		msg(url);
		var sessionID = getSessionID();
		
		var player = Nimbus.getPlayer( url, null, sessionID );
		
		player.setChromaKeyColor(0x00000000);
        //player.setVideoLayerBlendingMode("uniform_alpha");
		player.setVideoLayerBlendingMode("colorkey");
		player.setVideoLayerTransparency(1);
		//player.raiseVideoLayerToTop();
		player.setPictureFormat("Widescreen");
		player.setVideoLayerRect(0, 0, 1280, 720);
		player.setVideoLayerEnable(true);
		
		if (position>0)
			player.setPosition(position);
		
		player.play();
				
	}
	

	$("#overlay").fadeIn('slow');
	
	$("#VIDEO_nowplaying").text(ticketID);
	$("#VIDEO_player").text('play');
	
	return url;
}


function playAudio(audiofile)	{

	var ewf 		= ewfObject();
	
		var url = '<ChannelParams ChannelType="File" ChannelUsage="AudioOnly"><FileChannelParams><Playlist id="1" drive="Flash Drive" loop="1"><MPEG name="' + audiofile + '" location="audio" /></Playlist></FileChannelParams></ChannelParams>';
//alert(url);		
		var sessionID = getSessionID();
		
		var player = Nimbus.getPlayer( url, null, sessionID );
			
		if (position>0)
			player.setPosition(position);
		
		player.play();
					
	return url;
}

function getBOOKMARKS()	{
	
	var submenu = $("#K_submenu").attr("class");
	
	if (submenu=='movies')
		var bookmarksString = $("#JSON_bookmarks").text();
		else	
			var bookmarksString = $("#JSON_myprograms").text();	
	
if (submenu=='scenictv')
		var bookmarksString = $("#JSON_bookmarks").text();
	
	var bookmarks = JSON.parse(bookmarksString);
	
	return bookmarks;
}

function getSELECTION(video)	{
	
	var selectionString = $("#SELECTION_"+video).text();
	var selection = JSON.parse(selectionString);
	
	return selection;
}

function cancelVideo(video)	{
	
	var bookmarks = getBOOKMARKS();
	
	var ticketID = '';
	$.each(bookmarks['myprograms'], function(i,row){									
		if (video==row["tag"])	{
			ticketID = row["ticketID"];
		}
	});
	
	if (ticketID!='')	{
		var cancelDATA 	    = cancelAsset(ticketID);	// cancel purchase the movie/video
		var myprogramsDATA 	= getmyprogramsDATA();
		var allprogramsDATA = getAllProgramsDATA();
		bookmarks = getBOOKMARKS();
	}
	
	return bookmarks;
}

function updateVideo(key)	{
	
//	if (key=='CC')	{
//		setCC();
//		return;
//	}	else
//		if (key!='STOP'&&key!='PLAY'&&key!='PAUS'&&key!='ENTER'&&key!='FFWD'&&key!='RWND') 
//			return;
	
//	var suspendPosition = getSP();
//	var bookmarks 		= getBOOKMARKS();
//	var nowplaying  	= $("#VIDEO_nowplaying").text();
//	var pAsset    		= '';
//	
//	$.each(bookmarks['myprograms'], function(i,row){									
//		if (nowplaying==row["ticketID"])	{
//			pAsset = positionAsset(row["ticketID"], suspendPosition, row["poUID"]);
//		}
//	});
	
	var version = $("#K_version").text();
	if (version=='ENSEO' || version =="TCM")	{
		if (key=='STOP')	{	
 			stopVideo();			
		}	else
		if (key=='PLAY'||key=='PAUS')	{	
			var pp = $("#VIDEO_player").text();
			var player = Nimbus.getPlayer();
			if (player)	{
				if (pp=='pause')	{	
					player.play();
					$("#VIDEO_player").text('play');
					//msg('processed PLAY');
				}	else
					if (pp=='play')	{
						player.pause();
						$("#VIDEO_player").text('pause');
						suspendVideo();
						//msg('processed PAUS');
					}
			}
		}	else
		if (key=='FFWD')	{	
			var player = Nimbus.getPlayer();
			if (player)	{
				player.setSpeed(7);
			}
			$("#VIDEO_player").text('pause');
			//msg('processed FFWD');
		}	else
		if (key=='RWND')	{	
			var player = Nimbus.getPlayer();
			if (player)	{
				player.setSpeed(-7);
			}
			$("#VIDEO_player").text('pause');
			//msg('processed RWND');
		}
	}
	
	return;
}

function stopVideo()	{
	
	$("#overlay").hide();	
	
	suspendVideo();
	
//	var suspendPosition = getSP();
//	var bookmarks 		= getBOOKMARKS();
//	var nowplaying  	= $("#VIDEO_nowplaying").text();
//	var pAsset    		= '';
//	
//	$.each(bookmarks['myprograms'], function(i,row){									
//		if (nowplaying==row["ticketID"])	{
//			pAsset = positionAsset(row["ticketID"], suspendPosition, row["poUID"]);
//		}
//	});

	var version = $("#K_version").text();
	if (version=='ENSEO')	{
		var player = Nimbus.getPlayer();
		
		if (player)	{
			player.setVideoLayerEnable(false);
			player.lowerVideoLayerToBottom();
			player.stop();
			player.close();
		}	
	}
	
	$("#VIDEO_nowplaying").text('');
	$("#VIDEO_player").text('');

	//msg('processed STOP');
	
	return;
}

function suspendVideo()	{
	
	var suspendPosition = 0;
	
	var version = $("#K_version").text();
	if (version=='ENSEO')	{
		var player = Nimbus.getPlayer();
		if (player)	{
			suspendPosition = player.getPosition();
		}	
	}
	
	var bookmarks 		= getBOOKMARKS();
	var nowplaying  	= $("#VIDEO_nowplaying").text();
	var pAsset    		= '';
	
	$.each(bookmarks['myprograms'], function(i,row){									
		if (nowplaying==row["ticketID"])	{
			pAsset = positionAsset(row["homeID"],row["ticketID"],suspendPosition);
		}
	});	
	
	var myprogramsDATA = getmyprogramsDATA();
	
	if (version=='ENSEO')	{
		msg('Video/Movie Suspended at Position: '+suspendPosition);
	}
	
	return;	
}


//function getSP()	{
//	
//	var SP = 0;
//	
//	var version = $("#K_version").text();
//	if (version=='PROD')	{
//		SP = player.GetPosition();	
//	}
//	
//	return SP;	
//}


function eofVideo()	{	
	var version = $("#K_version").text();
	if (version=='PROD')	{		
		var player = Nimbus.getPlayer();
		if (player)	{
			var announcement = player.getRTSPAnnouncement();
			msg('Player Announcement: '+announcement);	
			if (announcement.indexOf('Beginning-of-Stream') != -1 || announcement.indexOf('End-of-Stream') != -1)	{			
				//--close the player
				stopVideo();
				//--go back to carousel
				$("#K_panel").removeClass('video');
				$("#K_panel").addClass('tertiary');
				$("#tertiaryLeft").show();
				$("#tertiaryRight").show();		
				$("#tertiary").show();
				msg('Player Terminated at End of Play');
			}
		}	
	}
	return;
}

// CHANGE BEG: 20120130-1000 bsears - added bookmarkCHECK function (PENDING)
function bookmarkCHECK(video)	{
	
	var bookmarks = getBOOKMARKS();
	var exists    = '';
	
	$.each(bookmarks['myprograms'], function(i,row){									
		if (video==row["tag"])	{
			exists = row["tag"];
		}
	});
	
	return exists;
}
// CHANGE END: 20120130-1000 bsears - added bookmarkCHECK function

