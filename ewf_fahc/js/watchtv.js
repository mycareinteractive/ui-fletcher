/********************************************************************************
 * @brief																		*
 * 		Watchtv, Musicradio, and/or Scenictv navigation functions               *
 *																				*
 * @author																		*
 *		Bill Sears\n															*
 *		Aceso\n																	*
 *		http://www.aceso.com\n													*
 *																				*
 * @modified																	*
 * 		Tami Seago, 02/05/2013, add info and error handling						*
 ********************************************************************************/

function buildgrid(grid)	{
// Build the EPG Grid	
	
	$("#K_panel").removeClass('secondary');
	$("#K_panel").addClass(grid);

	$("#K_grid").addClass(grid);
	
	$("#K_tomorrow").text(0);
	
	if (grid=='scenictv')	{
		$("#primary").hide();
		scenicTV();	
		return;
	}
	
	$("#primary").hide();
	$("#secondary").hide();
	$("#watchtv").show();
	
	var startpoint = getStartPoint();
	timeBegEnd(startpoint);
	channelBUILD();

	var gridHTML = getGridHTML(startpoint);

	$("#gridpanel.today").html(gridHTML.today);
	$("#gridpanel.channel").html(gridHTML.channel);
	$("#gridpanel.time").html(gridHTML.time);
	$("#gridpanel.program").html(gridHTML.program);
	
	$("#K_gridactive").addClass(gridHTML.channelA);
	$("#K_gridactive").text(gridHTML.timeA);
	
	var time = checkTimePos();
	
	$("#gridpanel.program #channel."+gridHTML.channelA+" div#"+time).addClass('active');
		
	var infoHTML = getBannerHTML(startpoint)
	
	$("#gridinfo").html(infoHTML.current);
		
	return;
}

function rebuildgrid()	{
	
	$("#K_tomorrow").text(0);
	$("#K_gridactive").removeAttr("class");
	$("#K_gridactive").text('');
	$("#gridpanel.today").html('');
	$("#gridpanel.channel").html('');
	$("#gridpanel.time").html('');
	$("#gridpanel.program").html('');
	
	var startpoint = getStartPoint();
	timeBegEnd(startpoint);
	channelBUILD();

	var gridHTML = getGridHTML(startpoint);

	$("#gridpanel.today").html(gridHTML.today);
	$("#gridpanel.channel").html(gridHTML.channel);
	$("#gridpanel.time").html(gridHTML.time);
	$("#gridpanel.program").html(gridHTML.program);
	
	$("#K_gridactive").addClass(gridHTML.channelA);
	$("#K_gridactive").text(gridHTML.timeA);
	
	var time = checkTimePos();
	
	$("#gridpanel.program #channel."+gridHTML.channelA+" div#"+time).addClass('active');
		
	var infoHTML = getBannerHTML(startpoint)
	
	$("#gridinfo").html(infoHTML.current);
		
	return;
}

function nextgrid(direction)	{
	
	var startpoint = getStartPoint();
	
	var timebeg  = $("#K_timebeg").text();
	var timeend  = $("#K_timeend").text();
	var tomorrow = $("#K_tomorrow").text() * 1;
	

	if (direction=='RIGHT')	{
		
		checkbeg = timebeg.substr(2,4);	
		checkend = timeend.substr(2,4);
		if (checkbeg=='2200'&&checkend=='2400')	{
			tomorrow++;
			if (tomorrow>2)	
				tommorow=2;
			$("#K_tomorrow").text(tomorrow);
			startpoint = getStartPoint();
			$("#K_timebeg").text('T'+startpoint['day']+'0000');
			$("#K_timeend").text('T'+startpoint['day']+'0200');
			//msg('end of day');	
		}	else	{
			var Td = '';
			var hh = '';
			var h  = 0;
			
			Td = timebeg.substr(0,2);
			h  = timebeg.substr(2,2)*1;
			h++;
			h++;
			hh = h;
			if (h<10) hh = '0'+h;
			
			$("#K_timebeg").text(Td+hh+'00');
			$("#K_gridactive").text(Td+hh+'00');
			
			Td = timeend.substr(0,2);
			h  = timeend.substr(2,2)*1;
			h++;
			h++;
			hh = h;
			if (h<10) hh = '0'+h;
			
			$("#K_timeend").text(Td+hh+'00');
		}
	}	else
	if (direction=='LEFT')	{
		
		checkbeg = timebeg.substr(2,4);	
		checkend = timeend.substr(2,4);
		if (checkbeg=='0000'&&checkend=='0200')	{
			tomorrow--;
			if (tomorrow<0)	
				tommorow=0;
			$("#K_tomorrow").text(tomorrow);
			startpoint = getStartPoint();
			startpoint['time'] = 'T'+startpoint['day']+'2200';
			$("#K_timebeg").text('T'+startpoint['day']+'2200');
			$("#K_timeend").text('T'+startpoint['day']+'2400');	
			//msg('beg of day');
		}	else	{
			var Td = '';
			var hh = '';
			var h  = 0;
			
			Td = timebeg.substr(0,2);
			h  = timebeg.substr(2,2)*1;
			h--;
			h--;
			hh = h;
			if (h<10) hh = '0'+h;
			
			$("#K_timebeg").text(Td+hh+'00');
			$("#K_gridactive").text(Td+hh+'00');
			
			Td = timeend.substr(0,2);
			h  = timeend.substr(2,2)*1;
			h--;
			h--;
			hh = h;
			if (h<10) hh = '0'+h;
			
			$("#K_timeend").text(Td+hh+'00');	
		}
	}	else
	if (direction=='UP')	{
		var channel = channelUP();
		$("#K_gridactive").removeAttr("class");
		$("#K_gridactive").addClass(channel);		
	}	else
	if (direction=='DOWN')	{
		var channel = channelDOWN();
		$("#K_gridactive").removeAttr("class");
		$("#K_gridactive").addClass(channel);
	}
	
	timebeg = $("#K_timebeg").text();
	timeend = $("#K_timeend").text();
	
	var gridHTML = getGridHTML(startpoint);

	$("#gridpanel.today").html(gridHTML.today);
	$("#gridpanel.channel").html(gridHTML.channel);
	$("#gridpanel.time").html(gridHTML.time);
	$("#gridpanel.program").html(gridHTML.program);
	
	var channel = $("#K_gridactive").attr("class");
	var time 	= checkTimePos();
	
	$("#gridpanel.program #channel."+channel+" div#"+time).addClass('active');
		
	var infoHTML = getBannerHTML(startpoint)
	
	$("#gridinfo").html(infoHTML.current);
		
	return;
	
}

function channelBUILD()	{
	
	var channels 	 = epgChannels();
	var count		 = 0;
	var firstchannel = '';
	var lastchannel  = '';
	
	$.each(channels['Channels'], function(i,row){
										  
		if (count==0)	firstchannel=row["channelID"];
		lastchannel=row["channelID"];
		
		count++;
		if (count>=1&&count<=7)	{
			$("#K_channel"+count).text(row["channelID"]);
		}
		
	});	
	
	$("#K_firstchannel").text(firstchannel);
	$("#K_lastchannel").text(lastchannel);
	
	return;
}

function channelDOWN()	{
	
	var channel7	= $("#K_channel7").text();	
	
	var channels 	= epgChannels();
	var count		= 8;

	$.each(channels['Channels'], function(i,row){
		if (row["channelID"]==channel7)
			count=-1;						  
		count++;
		if (count>=1&&count<=7)	{
			$("#K_channel"+count).text(row["channelID"]);
		}
	});	
	
	var channel = $("#K_channel1").text();
	
	return channel;
}

function channelUP()	{
	
	var channel1	 = $("#K_channel1").text();	
//	var firstchannel = $("#K_firstchannel").text();
//	if (channel1==firstchannel)	{
//		var channel = $("#K_channel7").text();
//		return;
//	}
	
	var channels = epgChannels();
	var count 	 = 0;
	var channelA = Array();
	var channelC = 0;

	$.each(channels['Channels'], function(i,row){
		channelA[channelC] = row["channelID"];								  							  
		if (row["channelID"]==channel1)
			count = channelC-7;
		channelC++;
	});	
	
	var i=0;
	channelC=count;
	count=7;
	for(i=1;i<=count;i++)	{
		$("#K_channel"+i).text(channelA[channelC]);
		channelC++;
	}
	
	var channel = $("#K_channel7").text();
	
	return channel;
}

function getStartPoint()	{
	
	var tomorrow = $("#K_tomorrow").text() * 1;
	
	var startpoint = Array();
	
	var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
	var months  = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	var munths  = ["01","02","03","04","05","06","07","08","09","10","11","12"];

	if (tomorrow==0)	{
		var d = new Date();
	}	else	{
			var today = new Date();
			d = new Date(today.getTime() + ((24 * 60 * 60 * 1000) * tomorrow) );
		}
	var dow		= d.getDay();
	var hour 	= d.getHours();
	var minute	= d.getMinutes();
	var month	= d.getMonth();
	var day		= d.getDate();
	var year 	= d.getFullYear();
	
	// set current information

	var dae = day;
	if (dae<'10')
		dae = '0'+dae;
	
	var ymd  	 = year+munths[month]+dae;
	var today    = year+'-'+munths[month]+'-'+dae;	
	
	//msg('ymd: '+ymd+' today: '+today);
	
	$("#K_ymd").text(ymd);
	$("#K_today").text(today);
	
	// load startpoint
		
	startpoint['day']  = dow;
	startpoint['dow']  = weekday[dow];
	startpoint['hour'] = hour;
	if (hour<10)
		startpoint['hour'] = '0'+hour;
	if (minute>=30)	
		startpoint['minute'] = '30';
		else	
			startpoint['minute'] = '00';
					
	if (tomorrow==0)	{
		startpoint['time']  = 'T'+startpoint['day']+startpoint['hour']+startpoint['minute'];
		startpoint['timestop']  = 'T'+startpoint['day']+startpoint['hour']+startpoint['minute'];
	}	else	{
			startpoint['time']  = 'T'+startpoint['day']+'0000';
			startpoint['timestop']  = '';
		}
	startpoint['date']  = months[month]+' '+day+', '+year;
	startpoint['ymd']   = ymd;
	startpoint['today'] = today;
	
	return startpoint;
}

function timeBegEnd(startpoint)	{
	
	var time = startpoint['time'];
	var hhmm = time.substr(2,4);
	//msg('hhmm: '+hhmm);
	var timebeg = '';
	var timeend = '';
	
	if (hhmm>='0000'&&hhmm<'0200')	{ timebeg='0000'; timeend='0200'; }	else
	if (hhmm>='0200'&&hhmm<'0400')	{ timebeg='0200'; timeend='0400'; }	else
	if (hhmm>='0400'&&hhmm<'0600')	{ timebeg='0400'; timeend='0600'; }	else
	if (hhmm>='0600'&&hhmm<'0800')	{ timebeg='0600'; timeend='0800'; }	else
	if (hhmm>='0800'&&hhmm<'1000')	{ timebeg='0800'; timeend='1000'; }	else
	if (hhmm>='1000'&&hhmm<'1200')	{ timebeg='1000'; timeend='1200'; }	else
	if (hhmm>='1200'&&hhmm<'1400')	{ timebeg='1200'; timeend='1400'; }	else
	if (hhmm>='1400'&&hhmm<'1600')	{ timebeg='1400'; timeend='1600'; }	else
	if (hhmm>='1600'&&hhmm<'1800')	{ timebeg='1600'; timeend='1800'; }	else
	if (hhmm>='1800'&&hhmm<'2000')	{ timebeg='1800'; timeend='2000'; }	else
	if (hhmm>='2000'&&hhmm<'2200')	{ timebeg='2000'; timeend='2200'; }	else
	if (hhmm>='2200'&&hhmm<='2400')	{ timebeg='2200'; timeend='2400'; }
	
	//msg('timebeg: '+timebeg+' timeend: '+timeend);
	
	$("#K_timebeg").text('T'+startpoint['day']+timebeg);
	$("#K_timeend").text('T'+startpoint['day']+timeend);	
	
}

function checkTimePos()	{
	
	var channel = $("#K_gridactive").attr("class");
	var time    = $("#K_gridactive").text();
	
	var checkvalid = $("#gridpanel.program #channel."+channel+" div#"+time).attr("id");
	if (!checkvalid)
		time = $("#K_timebeg").text();
	$("#K_gridactive").text(time);
	
	return time;
}

function verticalTimePos(channel)	{
	
	//var channel = $("#K_gridactive").attr("class");
	var time    = $("#K_gridactive").text();
	
	var checkvalid = $("#gridpanel.program #channel."+channel+" div#"+time).attr("id");
	if (!checkvalid)	{
		whereami = $("#gridpanel.program #channel."+channel+" div#"+time).prev().attr("id");
		if (whereami)
			time = whereami;
			else
				time = $("#K_timebeg").text();
	}
	
	$("#K_gridactive").text(time);
	
	return time;
}

function moveCurrent(key)	{
	
	var startpoint = getStartPoint();
	
	var whereami = '';
	
	var channel  = $("#K_gridactive").attr("class");
	var time     = $("#K_gridactive").text();
	
	if (key=='UP')	{	
		whereami = $("#gridpanel.program #channel."+channel).prev().attr("class");
		if(!whereami)	{
			nextgrid(key);
			return;
		}
		$("#gridpanel.program #channel."+channel+" div#"+time).removeClass('active');
		var time = verticalTimePos(whereami);
		$("#gridpanel.program #channel."+whereami+" div#"+time).addClass('active');
		$("#K_gridactive").removeAttr("class");
		$("#K_gridactive").addClass(whereami);
	}	else
	if (key=='DOWN')	{
		whereami = $("#gridpanel.program #channel."+channel).next().attr("class");
		if(!whereami)	{
			nextgrid(key);
			return;		
		}
		$("#gridpanel.program #channel."+channel+" div#"+time).removeClass('active');
		var time = verticalTimePos(whereami);
		$("#gridpanel.program #channel."+whereami+" div#"+time).addClass('active');
		$("#K_gridactive").removeAttr("class");
		$("#K_gridactive").addClass(whereami);	
	}	else
	if (key=='RIGHT')	{
		$("#gridpanel.program #channel."+channel+" div#"+time).removeClass('active');
		whereami = $("#gridpanel.program #channel."+channel+" div#"+time).next().attr("id");
		if	(!whereami)	{
			nextgrid(key);
			return;	
		}
		$("#gridpanel.program #channel."+channel+" div#"+whereami).addClass('active');
		$("#K_gridactive").text(whereami);
	}	else
	if (key=='LEFT')	{
		current  = $("#gridpanel.program #channel."+channel+" div#"+time).attr("id");
		if (startpoint['timestop']&&(current <= startpoint['timestop']))
			return;
		$("#gridpanel.program #channel."+channel+" div#"+time).removeClass('active');
		whereami = $("#gridpanel.program #channel."+channel+" div#"+time).prev().attr("id");
		if	(!whereami)	{
			nextgrid(key);
			return;	
		}	
		$("#gridpanel.program #channel."+channel+" div#"+whereami).addClass('active');
		$("#K_gridactive").text(whereami);
	}
	
	var infoHTML = getBannerHTML(startpoint)
	
	$("#gridinfo").html(infoHTML.current);
	
	return;
}

function gotochannel()	{
	
	var selectedchannel  = $("#K_gridactive").attr("class");
	var selectedtime     = $("#K_gridactive").text();	
	
	channel = $("#gridpanel.channel #"+selectedchannel).text();
	time 	= $("#gridpanel.time #"+selectedtime).text();
	program = $("#gridpanel.program #channel."+selectedchannel+" #"+selectedtime).text();
		
	var grid = $("#K_grid").attr("class");
	$("#K_panel").removeClass(grid);
	$("#K_panel").addClass('tv');
	$("#watchtv").hide();
	
	var url = playTV(selectedchannel);

	return;
}

function getBannerHTML(startpoint)	{
	
	var activechannel  = $("#K_gridactive").attr("class");
	var activetime     = $("#K_gridactive").text();
		
	var channel = $("#gridpanel.channel #"+activechannel).text();
	var time 	= $("#gridpanel.time #"+activetime).text();
	var program = $("#gridpanel.program #channel."+activechannel+" div#"+activetime).text();
	var classx 	= $("#gridpanel.program #channel."+activechannel+" div#"+activetime).attr("class");
	var title   = $("#gridpanel.program #channel."+activechannel+" div#"+activetime).attr("title");

	var length = classx.split(' ');
	if (length[0]=='D30')
		duration = '30 Minutes';
	else
	if (length[0]=='D60')
		duration = '1 Hour';
	else
	if (length[0]=='D90')
		duration = '1 Hour 30 Minutes';
	else
	if (length[0]=='D120')
		duration = '2 Hours';
//	else
//	if (length[0]=='D150')
//		duration = '2 Hours 30 Minutes';
//	else
//	if (length[0]=='D180')
//		duration = '3 Hours';
//	else
//	if (length[0]=='D210')
//		duration = '3 Hours 30 Minutes';
//	else
//	if (length[0]=='D240')
//		duration = '4 Hours';
//	else
//	if (length[0]=='D270')
//		duration = '4 Hours 30 Minutes';
//	else
//	if (length[0]=='D300')
//		duration = '5 Hours';	
	
	var startHH = activetime.substr(2,2)*1;
	var startMM = activetime.substr(4,2)*1;
	var hh   = 0;
	var mm   = 0;
	
	if (startMM==0)	{
		if (length[0]=='D30')	{ hh = 0; mm = 30;	}	else
		if (length[0]=='D60')	{ hh = 1; mm = 00;	}	else
		if (length[0]=='D90')	{ hh = 1; mm = 30;	}	else
		if (length[0]=='D120')	{ hh = 2; mm = 00;	}
	}	else
	if (startMM==30)	{
		if (length[0]=='D30')	{ hh = 1; mm = -30;	}	else
		if (length[0]=='D60')	{ hh = 1; mm = 00;	}	else
		if (length[0]=='D90')	{ hh = 2; mm = -30;	}	else
		if (length[0]=='D120')	{ hh = 2; mm = 00;	}
	}
	
	var endHH = startHH + hh;
	var endMM = startMM + mm;
	
	var mm0  = '';
	var ampm = '';
	
	hh   = startHH;
	mm   = startMM;
	mm0  = '';	
	ampm = 'am';
	if (hh>11)	{ 
		ampm = 'pm';
	}
	if (hh>12)	{ 
//		ampm = 'pm';
		hh   = hh - 12;
	}
	if (hh==0)	{
		hh=12;	
	}
	if(mm<10) mm0='0';
	var begtime = hh+':'+mm0+mm+ampm;
	hh   = endHH;
	mm   = endMM;
	mm0  = '';
	ampm = 'am';
	if (hh>11)	{ 
		ampm = 'pm';
	}
	if (hh==24)	{ 
		ampm = 'am';
	}
	if (hh>12)	{ 
//		ampm = 'pm';
		hh   = hh - 12;
	}
	if (hh==0)	{
		hh=12;	
	}	
	if(mm<10) mm0='0';
	var endtime = hh+':'+mm0+mm+ampm;
	var begend = begtime+' - '+endtime;

	var current = '';
	current = current+'<div id="current">';
	
	current = current+'<p class="title">'+title+'</p>';
	//current = current+'<p class="title">Watch TV</p>';
	current = current+'<p class="channel">'+channel+'</p>';
	current = current+'<p class="time">'+begend+'<br/>' + duration + '</p>';
	//current = current+'<p class="time">'+duration+'</p>'
	current = current+'<p class="message">Press MENU For More Features</p>';
	current = current+'<p class="date">'+startpoint['date']+'</p>';
	current = current+'</div>';

	var infoHTML = Array();
	var today 	 = $("#K_today").text();	
	infoHTML['channel'] = today;
	infoHTML['time'] 	= time;
	infoHTML['current'] = current;

	return infoHTML;
}

function getGridHTML(startpoint)	{
	
	var today 	= '<div id="today">'+startpoint['dow']+'</div>';
	
	var channel = buildChannelDATA();
	var time 	= buildTimeDATA(startpoint['day']);
	var program = buildProgramDATA(startpoint['day']);
	
	var channelA = $("#K_channel1").text();
	var timeA    = startpoint['time'];
	
	var gridHTML = Array();

	gridHTML['channelA'] = channelA;
	gridHTML['timeA']    = timeA;
	gridHTML['today'] 	 = today;
	gridHTML['channel']  = channel;
	gridHTML['time'] 	 = time;
	gridHTML['program']  = program;

	return gridHTML;
}	

function buildChannelDATA()	{
	
	var channels 	= epgChannels();
	var channel  	= '';
	
	var channel1	 = $("#K_channel1").text();
	var channel2	 = $("#K_channel2").text();
	var channel3	 = $("#K_channel3").text();
	var channel4	 = $("#K_channel4").text();
	var channel5	 = $("#K_channel5").text();
	var channel6	 = $("#K_channel6").text();
	var channel7	 = $("#K_channel7").text();	
	

	$.each(channels['Channels'], function(i,row){
		if (row["channelID"]==channel1||row["channelID"]==channel2||row["channelID"]==channel3||row["channelID"]==channel4||row["channelID"]==channel5||row["channelID"]==channel6||row["channelID"]==channel7)	{
			channel = channel+'<div id="'+row["channelID"]+'">'+row["channelNumber"]+' '+row["channelName"]+'</div>';
		}
	});	
	
	$("#EPG_channel").html(channel);
	
	return channel;
}

function buildTimeDATA()	{
	
	var time = '';
	var Td 	 = '';
	var h	 = 0;
	var ap	 = '';
	var hh   = '';
	
	var timebeg = $("#K_timebeg").text();
	
	Td = timebeg.substr(0,2);
	h = timebeg.substr(2,2)*1;
//	var hh = h;
	ap = 'pm';
	if (h<12) 	ap = 'am';
	if (h>12)	h = h-12;
	if (h==0)   h = 12;
	if (h<10) 	hh = '0'+h;
	//msg(hh);
	
	time = time+'				<div id="'+Td+hh+'00">'+h+':'+'00'+ap+'</div>';
	time = time+'				<div id="'+Td+hh+'30">'+h+':'+'30'+ap+'</div>';
	
	h++;
//	var hh = h;
//	ap = 'pm';
	if (h==12) 	ap = 'pm';
	if (h>12)	h = h - 12;
	if (h<10) 	hh = '0'+h;
	
	time = time+'				<div id="'+Td+hh+'00">'+h+':'+'00'+ap+'</div>';
	time = time+'				<div id="'+Td+hh+'30">'+h+':'+'30'+ap+'</div>';	
	
	$("#EPG_time").html(time);
	
	return time;
}

function buildProgramDATA(wtf)	{

	var channelDATA = getChannelDATA();
	
	var channel  	 = '';
	var channelfile  = '';
	var program  	 = '';
	var pgmname  	 = '';
	var namelen  	 = 0;
	var c 			 = 0;
	var x 			 = 0;
	var channel1	 = $("#K_channel1").text();
	var channel2	 = $("#K_channel2").text();
	var channel3	 = $("#K_channel3").text();
	var channel4	 = $("#K_channel4").text();
	var channel5	 = $("#K_channel5").text();
	var channel6	 = $("#K_channel6").text();
	var channel7	 = $("#K_channel7").text();
	
	// get channels
	
	var channels = epgChannels();
	
	var channelA = Array();
	$.each(channels['Channels'], function(i,row){
		if (row["channelID"]==channel1||row["channelID"]==channel2||row["channelID"]==channel3||row["channelID"]==channel4||row["channelID"]==channel5||row["channelID"]==channel6||row["channelID"]==channel7)	{					  
			channelA[c] = row["channelID"];
			c++;
		}
	});	
	x = c - 1;
	
	for(c=0;c<=x;c++)	{ 
		channel = channelA[c];	
		channelfile = epgChannel(channel);
		program = program+'<div id="channel" class="'+channel+'">';
		$.each(channelfile.programs, function(i,row) {
			if	(!row["EOF"])	{
				
				pgmname = row["programName"];
				//temp remove to pull data
				//pgmname = 'Watch TV'
				namelen = pgmname.length;
				if(row["duration"]=='D30'&&namelen>12) pgmname = pgmname.substr(0,12)+'...';																				
				if(row["duration"]=='D60'&&namelen>32) pgmname = pgmname.substr(0,32)+'...';				
				
				program = program+'<div id="T'+wtf+row["startTime"]+'" class="'+row["duration"]+'" title="'+row["programName"]+'" >'+pgmname+'</div>';
			}
		});
		program = program+'</div>';
	}
	
	$("#EPG_program").html(program);
		
	return program;
}

function calcDuration(startTime,endTime)	{
	
	var timebeg  = $("#K_timebeg").text();
	var timeend  = $("#K_timeend").text();
	
	var begYMD	 = '';
	var begHH	 = '';
	var begMM	 = '';
	var begHHMM	 = '';
	var endYMD	 = '';
	var endHH	 = '';
	var endMM	 = '';
	var endHHMM	 = '';
	var begtime	 = '';
	var endtime	 = '';
	var hh		 = '';
	var mm		 = '';
	var ampm	 = '';
	var beg		 = 0;
	var end		 = 0;
	var difference	= 0;
	var duration	= 0;

	begYMD 	= startTime.substr(0,10);
	begHH	= startTime.substr(11,2);
	begMM 	= startTime.substr(14,2);

	endYMD 	= endTime.substr(0,10);
	endHH 	= endTime.substr(11,2);
	endMM 	= endTime.substr(14,2);
	
	if (begMM!='00'&&begMM!='30')	{
		//msg(startTime+' begMM before: '+begMM);
		if (begMM>'14'&&begMM<'46')	
			begMM='30';
		else
			begMM='00';
		//msg(startTime+' begMM after: '+begMM);
	}
	if (endMM!='00'&&endMM!='30')	{
		//msg(endTime+' endMM before: '+endMM);
		if (endMM>'14'&&endMM<'46')	
			endMM='30';
		else
			endMM='00';
		//msg(endTime+' endMM after: '+endMM);
	}	
	
	begHHMM = (begHH+begMM);
	endHHMM = (endHH+endMM);
	if (begHHMM!='0000'&&endHHMM=='0000')	
		endHHMM='2400';
//	if (endHHMM<begHHMM)	
//		endHHMM='2400';	
	
	difference = (endHHMM - begHHMM)*1;
	
	if (difference==30) 	{ duration=30;  }	else
	if (difference==70) 	{ duration=30;  }	else
	if (difference==100) 	{ duration=60;  }	else
	if (difference==130) 	{ duration=90;  }	else
	if (difference==170) 	{ duration=90;  }	else
	if (difference==200) 	{ duration=120; }	else
		{
		//msg('ERROR: '+startTime+' '+endTime+' calc result: '+difference);
		duration=30;
		}
	
	//msg(startTime+' '+endTime+' calc result: '+difference+' duration: '+duration);
	
	hh   = begHH;
	mm   = begMM;
	ampm = 'am';
	if (hh>11)	{ 
		ampm = 'pm';
	}
	if (hh>12)	{ 
//		ampm = 'pm';
		hh   = hh - 12;
	}
	if (hh==0)	{
		hh=12;	
	}	
	begtime = hh+':'+mm+ampm;
	
	hh   = endHH;
	mm   = endMM;
	ampm = 'am';
	if (hh>11)	{ 
		ampm = 'pm';
	}
	if (hh>12)	{ 
//		ampm = 'pm';
		hh   = hh - 12;
	}
	if (hh==0)	{
		hh=12;	
	}	
	endtime = hh+':'+mm+ampm;	
	
	var durationA = Array();
	durationA['duration'] = duration;
	durationA['begHHMM']  = begHHMM;
	durationA['endHHMM']  = endHHMM;
	durationA['begtime']  = begtime;
	durationA['endtime']  = endtime;

	return durationA;
}

function getChannelDATA()	{
	
	//msg('arrived getChannelDATA');
	
	var client = $("#K_client").text();
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: activate musicradio
	var panel = $("#K_panel").attr("class");
// CHANGE END: 20120214-1300 bsears - change via Dennis: activate musicradio

	var channels = epgChannels();
	
	// get channels
	
	var channel = '';
	var program = '';
	var swedish = '';
	
	var channel1	 = $("#K_channel1").text();
	var channel2	 = $("#K_channel2").text();
	var channel3	 = $("#K_channel3").text();
	var channel4	 = $("#K_channel4").text();
	var channel5	 = $("#K_channel5").text();
	var channel6	 = $("#K_channel6").text();
	var channel7	 = $("#K_channel7").text();	
	
	var c = 0;
	var x = 0;
	
	// get channels

	var channelA = Array();
	$.each(channels['Channels'], function(i,row){	
		if (row["channelID"]==channel1||row["channelID"]==channel2||row["channelID"]==channel3||row["channelID"]==channel4||row["channelID"]==channel5||row["channelID"]==channel6||row["channelID"]==channel7)	{								  		
			channelA[c] = row["channelID"];
			channel = channelA[c];
			$("#CHANNEL_"+channel).text('{ "channel":"'+channel+'", "programs": [ ');
			c++;
		}
	});	
	x = c - 1;	
		
	// build filename
	
	var ymd 	 = $("#K_ymd").text();
	var today 	 = $("#K_today").text();	
	var timebeg  = $("#K_timebeg").text();
	var hhmm     = timebeg.substr(2,4);
	var hh       = hhmm.substr(0,2);
	var filename = ymd+hhmm+'.txt';
	var pgmdate  = '';
	var source   = '';
	var comma    = '';
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: activate musicradio
	var nodata   = '';
// CHANGE END: 20120214-1300 bsears - change via Dennis: activate musicradio	

	// process program file
	
	
	epgfile = getProgramFILE(filename);	
	//msg(filename);
	if(epgfile!='')	{
		for(c=0;c<=x;c++)	{ 
			channel = channelA[c];	
			
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: activate musicradio
			// no programming for music radio
			if (panel=='musicradio')	{	
				swedish = generateSwedish(channel,today,hh);
				$("#CHANNEL_"+channel).append(swedish);			
//			}	else
			// exception processing
//			if (client=='SWEDISH'&&(channel=='C1'||channel=='C60000076'||channel=='C60000053'||channel=='C60000087'||channel=='C60000083'||channel=='C60000072'))	{
//				swedish = generateSwedish(channel,today,hh);
//				$("#CHANNEL_"+channel).append(swedish);
// CHANGE END: 20120214-1300 bsears - change via Dennis: activate musicradio
			}	else	{
				// normal processing
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: activate musicradio
				nodata = '';
// CHANGE END: 20120214-1300 bsears - change via Dennis: activate musicradio
				source   = channel;
				//if (client=='ACESO'||client=='FAHC')  source = 'C';			
				$.each(epgfile.ListOfChannel[source], function(i,row){
					if (row["programName"])	{
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: activate musicradio
						nodata = 'X';
// CHANGE END: 20120214-1300 bsears - change via Dennis: activate musicradio
						pgmdate = row["startTime"].substr(0,10);
						d = calcDuration(row["startTime"],row["endTime"]);
						if (d.begHHMM!=d.endHHMM)	{
							program = ' { "programName":"'+row["programName"]+'", "programDate":"'+pgmdate+'", "programTime":"'+d.begtime+'-'+d.endtime+'", "startTime":"'+d.begHHMM+'", "endTime":"'+d.endHHMM+'", "duration":"D'+d.duration+'" }, ';
							$("#CHANNEL_"+channel).append(program);
						}
					}
				});
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: activate musicradio
				if (nodata=='')	{
					swedish = generateSwedish(channel,today,hh);
					$("#CHANNEL_"+channel).append(swedish);							
				}
// CHANGE END: 20120214-1300 bsears - change via Dennis: activate musicradio				
			}
			$("#CHANNEL_"+channel).append(' { "EOF":"EOF"} ] }');
		}
	}
	
	var repairDATA   = repairProgramDATA();
	
	var version = $("#K_version").text();
	if (version=='TEST')	{
		//var validateDATA = validateProgramDATA();
	}	
	
	return 'success';
}

function getProgramFILE(filename)	{
	
	var ewf 	= ewfObject();
	var url		= ewf.epgFilePath+filename;
	var args    = '';
	var epgfile = ajaxJSON(url,args);

	return epgfile; 
}

function generateSwedish(channel,programDate,hh)	{
	
	var client = $("#K_client").text();

	var swedish 	= '';
	var pgmname 	= 'Program Information Not Available';
	var startTime 	= '';
	var endTime 	= '';
	var hh0			= '0';
	var hhmm		= '';
	
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: activate musicradio

	var channels = epgChannels();
	$.each(channels['Channels'], function(i,row){	
		if (row["channelID"]==channel)	{								  
			pgmname = row["channelName"];
		}
	});		
	
//	if (client=='SWEDISH')	{
//		if (channel=='C1')			pgmname='Swedish Issaquah';
//		if (channel=='C60000076')	pgmname='Channel Guide';
//		if (channel=='C60000053')	pgmname='MET Opera Radio';
//		if (channel=='C60000087')	pgmname='Food Network';
//		if (channel=='C60000083')	pgmname='JOE TV';
//		if (channel=='C60000072')	pgmname='MBC';
//	}
	
// CHANGE END: 20120214-1300 bsears - change via Dennis: activate musicradio
	
	if (hh>9) { hh0 = ''; };
	hhmm = hh0+hh+':00';
	startTime = programDate+' '+hhmm;
	hh++;
	hh++;
	if (hh>9) { hh0 = ''; };
	hhmm = hh0+hh+':00';
	endTime	= programDate+' '+hhmm;
	
	d = calcDuration(startTime,endTime);
	
	swedish = ' { "programName":"'+pgmname+'", "programDate":"'+programDate+'", "programTime":"'+d.begtime+'-'+d.endtime+'", "startTime":"'+d.begHHMM+'", "endTime":"'+d.endHHMM+'", "duration":"D'+d.duration+'" }, ';
		
	return swedish
}

function repairProgramDATA()	{
	
	var timebeg 	= $("#K_timebeg").text();
	timebeg = timebeg.substr(2,4);
	var timeend 	= $("#K_timeend").text();
	timeend = timeend.substr(2,4);
	var channel1	= $("#K_channel1").text();
	var channel2	= $("#K_channel2").text();
	var channel3	= $("#K_channel3").text();
	var channel4	= $("#K_channel4").text();
	var channel5	= $("#K_channel5").text();
	var channel6	= $("#K_channel6").text();
	var channel7	= $("#K_channel7").text();

	// get channels
	
	var channels = epgChannels();
	
	var channel = '';
	var program = '';
	var c = 0;
	var x = 0;
	
	// get channels

	var channelA = Array();
	$.each(channels['Channels'], function(i,row){	
		if (row["channelID"]==channel1||row["channelID"]==channel2||row["channelID"]==channel3||row["channelID"]==channel4||row["channelID"]==channel5||row["channelID"]==channel6||row["channelID"]==channel7)	{								  
			channelA[c] = row["channelID"];
			c++;
		}
	});	
	x = c - 1;
	
	var startFull	= '';
	var endFull		= '';
	var comma 		= '';
	var	HH			= '';
	var	MM			= '';
	var	beg    = '';
	var	end    = '';
	var	dur    = '';

	var d = Array();
	
	for(c=0;c<=x;c++)	{ 
		channel = channelA[c];	
		channelfile = epgChannel(channel);
		
		startFull	= '';
		endFull		= '';
		comma  		= '';
		HH			= '';
		MM			= '';
		beg    		= '';
		end    		= '';
		dur    		= '';
		
		program = '{ "channel":"'+channel+'", "programs": [';
		$.each(channelfile.programs, function(i,row){
			if (row["programName"])	{											  
				if (row["startTime"]!=row["endTime"])	{
					
					beg	= row["startTime"];
					end = row["endTime"];
					dur = row["duration"];
					
					//msg('beg: '+beg+' end: '+end+' dur: '+ dur);
					
					if (timebeg=='0000'&&row["startTime"]<'2400'&&row["endTime"]<row["startTime"])	{
						
						//msg('0000-2330');
						
						beg = timebeg;

						HH			= beg.substr(0,2);
						MM 			= beg.substr(2,2);
						startFull	= row["programDate"]+' '+HH+':'+MM;
						HH			= end.substr(0,2);
						MM 			= end.substr(2,2);
						endFull		= row["programDate"]+' '+HH+':'+MM;
						d = calcDuration(startFull,endFull);						
							
						beg =	d.begHHMM;
						end =	d.endHHMM;
						dur =	'D'+d.duration;
					}	else					
					if (row["startTime"]<timebeg||row["endTime"]>timeend)	{
						
						//msg('lt gt');
	
						if (row["startTime"]<timebeg)
							beg = timebeg;
						if (row["endTime"]>timeend)
							end = timeend;
						HH			= beg.substr(0,2);
						MM 			= beg.substr(2,2);
						startFull	= row["programDate"]+' '+HH+':'+MM;
						HH			= end.substr(0,2);
						MM 			= end.substr(2,2);
						endFull		= row["programDate"]+' '+HH+':'+MM;
						d = calcDuration(startFull,endFull);						
							
						beg =	d.begHHMM;
						end =	d.endHHMM;
						dur =	'D'+d.duration;
					}	
				
					
					if (beg!=end)	{
						program = program+comma+' { "programName":"'+row["programName"]+'", "programDate":"'+row["programDate"]+'", "programTime":"'+row["programTime"]+'", "startTime":"'+beg+'", "endTime":"'+end+'", "duration":"'+dur+'" }';
						comma = ',';
					}
				}
			}
		});
		program = program+' ] }';
		$("#CHANNEL_"+channel).text(program);
	}
	
	return 'success';
}

function splitProgram(programName,programDate,programTime,startTime,endTime,duration,program,comma)	{
	
	//msg('SPLIT... programName: '+programName+' programDate: '+programDate+' programTime: '+programTime+' startTime: '+startTime+' endTime: '+endTime+' duration: '+duration);

	var startFull 	= '';
	var endFull  	= '';
	var saveTime  	= endTime;
	//var programTime = d.begtime+'-'+d.endtime;
	//var duration	= d.duration;
						
	var hh			= startTime.substr(0,2);
	var mm			= startTime.substr(2,2);
//	var hh			= parseInt(hh);
//	var mm			= parseInt(mm);
	var hh0			= '0';
	var mm0			= '0';
	var hhmm		= '';
	var d = Array();
	
	//msg('after parse... '+startTime+' '+endTime+' hh mm: '+hh+' '+mm+' duration: '+duration);
	
	for(i=0;i>=0;i++)	{
		
		hh0	= '';
		mm0	= '';
	
//		if (hh<10) { hh0 = '0'; };
//		if (mm<10) { mm0 = '0'; };
//		if (hh>=0&&hh<=9) { hh = '0'+hh; };
//		if (mm>=0&&mm<=9) { mm = '0'+mm; };
		hhmm = hh0+hh+mm0+mm;
		startFull = programDate+' '+hhmm;
		hh++;
		hh++;
//		if (hh<10) { hh0 = '0'; };
		if (hh>=0&&hh<=9) { hh = '0'+hh; };
//		if (mm>=0&&mm<=9) { mm = '0'+mm; };
		hhmm = hh0+hh+mm0+mm;
		endFull = programDate+' '+hhmm;
	
		d = calcDuration(startFull,endFull);
		
		//msg('after calcDuration... '+startFull+' '+endFull+' startDur: '+d.begHHMM+' endDur: '+d.endHHMM+' duration: '+d.duration);
						
		program = program+comma+' { "programName":"'+programName+'", "programDate":"'+programDate+'", "programTime":"'+programTime+'", "startTime":"'+d.begHHMM+'", "endTime":"'+d.endHHMM+'", "duration":"D'+d.duration+'" }';
		comma = ',';
		
		duration = duration - 120;
		if (duration < 120) break;
		
		hh	= d.endHHMM.substr(0,2);
		mm	= d.endHHMM.substr(2,2);
//		if (hh>=0&&hh<=9) { hh = '0'+hh; };
//		if (mm>=0&&mm<=9) { mm = '0'+mm; };
		
	}
	
	if (duration != 0)	{
		
		//if (duration==30||duration==90)	mm=0;
		
		hh0	= '';
		mm0	= '';
		
		hh	= d.endHHMM.substr(0,2);
		mm	= d.endHHMM.substr(2,2);
//		if (hh>=0&&hh<=9) { hh = '0'+hh; };
//		if (mm>=0&&mm<=9) { mm = '0'+mm; };
		
//		if (hh<10) { hh0 = '0'; };
//		if (mm<10) { mm0 = '0'; };
		hhmm = hh0+hh+mm0+mm;
		startTime = programDate+' '+hhmm;
		endTime   = programDate+' '+saveTime;
		
		//msg('calcDuration: '+startTime+' '+endTime);
		
		d = calcDuration(startTime,endTime);	
						
		program = program+comma+' { "programName":"'+programName+'", "programDate":"'+programDate+'", "programTime":"'+programTime+'", "startTime":"'+d.begHHMM+'", "endTime":"'+d.endHHMM+'", "duration":"D'+d.duration+'" }';
		comma = ',';		
		
	}

	return program;
	
}

function validateProgramDATA()	{
	
	var channels = epgChannels();
	
	var timebeg 	= $("#K_timebeg").text();
	timebeg = timebeg.substr(2,4);
	var timeend 	= $("#K_timeend").text();
	timeend = timeend.substr(2,4);
	var channel1	= $("#K_channel1").text();
	var channel2	= $("#K_channel2").text();
	var channel3	= $("#K_channel3").text();
	var channel4	= $("#K_channel4").text();
	var channel5	= $("#K_channel5").text();
	var channel6	= $("#K_channel6").text();
	var channel7	= $("#K_channel7").text();
	
	var channel 	= '';
	var channelfile = '';
	var	startPrev	= '';
	var	endPrev		= '';
	var c = 0;
	var x = 0;
	
	// get channels

	var channelA = Array();
	$.each(channels['Channels'], function(i,row){	
		if (row["channelID"]==channel1||row["channelID"]==channel2||row["channelID"]==channel3||row["channelID"]==channel4||row["channelID"]==channel5||row["channelID"]==channel6||row["channelID"]==channel7)	{
			channelA[c] = row["channelID"];
			c++;
		}
	});	
	x = c - 1;
	
	for(c=0;c<=x;c++)	{ 
		channel = channelA[c];	
		channelfile = epgChannel(channel);

		startPrev	= '';
		endPrev		= '';

		$.each(channelfile.programs, function(i,row){
			if (row["EOF"])	{
				if (endPrev!=timeend)
					msg('ERROR for channel '+channel+', '+row["programName"]+' - endTime not 2400: '+row["startTime"]);
				startPrev = '';
				endPrev   = '';										  							  
			}	else
			if (row["programName"])	{
				//if (channel=='C60000038')
					//msg(row["startTime"]+' '+row["endTime"]+' '+row["programName"]);
				
				if (startPrev=='')	{
					if (row["startTime"]!=timebeg)
						msg('ERROR for channel '+channel+', '+row["programName"]+' - startTime not 0000: '+row["startTime"]);
					startPrev = row["startTime"];
					endPrev   = row["endTime"];
				}	else	{
						if (row["startTime"]!=endPrev)
							msg('ERROR for channel '+channel+', '+row["programName"]+' - startPrev: '+startPrev+' '+endPrev+' startTime: '+row["startTime"]);
						startPrev = row["startTime"];
						endPrev   = row["endTime"];						
					}
			}
		});

	}
	
	return 'success';
}

function cleanupTV()	{
	
	$("#K_tomorrow").text('');
	$("#K_today").text('');
	$("#K_ymd").text('');
	$("#K_firstchannel").text('');
	$("#K_lastchannel").text('');
	$("#K_grid").text('');
	$("#K_grid").removeAttr('class');
	$("#K_gridactive").text('');
	$("#K_gridactive").removeAttr('class');
	$("#K_gridpos").text('');
	$("#K_gridpos").removeAttr('class');
	$("#K_channel1").text('');
	$("#K_channel2").text('');
	$("#K_channel3").text('');
	$("#K_channel4").text('');
	$("#K_channel5").text('');
	$("#K_channel6").text('');
	$("#K_channel7").text('');
	$("#K_timebeg").text('');
	$("#K_timeend").text('');
	
	$("#gridinfo").text('');
	$("#gridbg").text('');
	$("#gridcover").text('');
	
	$("#gridpanel.today").text('');
	$("#gridpanel.channel").text('');
	$("#gridpanel.time").text('');
	$("#gridpanel.program").text('');
	
	$("#channelinfo").text('');
	
	$("#EPG_today").text('');
	$("#EPG_channel").text('');
	$("#EPG_time").text('');
	$("#EPG_program").text('');
		
	return true;
}
