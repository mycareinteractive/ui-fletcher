/********************************************************************************
 * @brief																		*
 * 		Power Functions - What happens when power on off is pressed             *
 *																				*
 * @author																		*
 *		Bill Sears\n															*
 *		Aceso\n																	*
 *		http://www.aceso.com\n													*
 *																				*
 * @modified																	*
 * 		Tami Seago, 02/05/2013, add info and error handling						*
 *																				*
 ********************************************************************************/

function checkPower() {
	
	msg('Checking power state of TV...');
	
	var onoff = 'OFF';
	
	var version = $("#K_version").text();
	if (version=='ENSEO')	{
		var TVController = Nimbus.getTVController();
		if (TVController) {
			var power = TVController.getPower();
			if (power==true)	
				onoff = 'ON';
		}
	} else 
	if (version=='NEBULA')	{
		// TV Controller code needs to be added when code complete
	}
	
	msg('TV is '+onoff);

	return onoff;
}

function setVolume() {
	
	msg('Setting Volume');
	
	var TVController = Nimbus.getTVController();
	if (TVController) {
		TVController.setVolume(55);
	}
	return;
}

function getPower() {
	
	msg('Getting power state');
	
	var TVController = Nimbus.getTVController();
	if (TVController != null) {
		TVController.setVolume(55);
		$("#K_power").text('ON');
		return TVController.getPower();
	}
	$("#K_power").text('OFF');
	return false;
}

function powerON() {
	
	msg('Setting power state to ON');
	
	var TVController = Nimbus.getTVController();
	if (TVController != null) {
		TVController.setVolume(55);
		TVController.setPower(true);
		$("#K_power").text('ON');
	} else {
			msg('ERROR: No Valid TV Controller');
		}
		
	return;
}

function powerOFF() {
	
	msg('Setting power state to OFF');
	
	var TVController = Nimbus.getTVController();
	if (TVController != null) {
		TVController.setPower(false);
		$("#K_power").text('OFF');
	} else {
			msg('ERROR: No Valid TV Controller');
		}
		
	return;
}

function togglePower() {
	
	var power = $("#K_power").text();
	
	var TVController = Nimbus.getTVController();
	if (TVController != null) {
		if (power=='ON') {
			powerOFF();
			return false;
		} else {
				powerON();
				return false;
			}
	}
}